# InfoTheoryThesis

Repo for information-theory related scripts written as a part of Masters thesis 2019. 

## Updates:

\[Sun Jun 14 19:19:07 IST 2020\] Moved bet hedging files to a [separate repo](https://gitlab.com/aashishsatya/bethedging).

\[Fri May 17 01:22:46 IST 2019\] Yes, they were a residue of the approximation. Checking for small values of N using Greedy method indicates they were so.  

\[Wed May 15 15:19:42 IST 2019\] Plots for worst case vs actual achievable seem to give…questionable results. Probably a residue of the fact that the algorithm approximates the solution from Lagrange method. Onwards to checking that.
