import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.misc
import seaborn as sns
import pandas as pd

def compute_updated_exp_val(current_score, N, p, i, j):
    
    """
    Returns <log f>_g as of the entries in the n_matrix
    """
    
    global contrib_msg_to_score
    
    # what has got invalidated is the component resulting from the ith message
    
    updated_score = current_score - msg_probs[i] * math.log2(contrib_msg_to_score[i])
    updated_score += msg_probs[i] * math.log2(contrib_msg_to_score[i] + (p ** j) * ((1 - p)) ** (N - j))
    return updated_score

M = 100
pvals = list(np.arange(0.05, 0.46, 0.03))
# pvals = [0.1, 0.2] # for testing
high_noises = pvals[:]

tvals = list(np.arange(0.5, 3.6, 0.5))
# tvals = [2]

N = 10

limits = np.array([scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)])
contrib_msg_to_score = [0] * M # contribution of each message to the score

for t in tvals:

    print('Computing for t =', t, '...')

    hit_matrix_dict = {}
    
    for high_noise_index in range(len(high_noises)):
        
        high_noise = high_noises[high_noise_index]

        hit_matrix_dict[high_noise] = {}
    
        contrib_msg_to_score = [0] * M # contribution of each message to the score
        
        msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
        #    msg_probs = np.array([1.0] * M)   # for sanity checks
        msg_probs = list(msg_probs)
        msg_probs.sort(reverse = True)
        msg_probs = np.array(msg_probs)
        sum_probs = sum(msg_probs)
        msg_probs /= sum_probs
        
        prepn_for_worst_case = []
        actual_achievable = []
        
        print('Computing for optimized prob =', high_noise, '...')
        
        # compute for high noise regime
        
        p = high_noise  # for now
        high_noise_str = "{:.2f}".format(high_noise)  # had to do this because 0.150000000000001 etc
            
        high_noise_matrix = np.zeros((M, N + 1))
        filled_vol = 0
    
        # at least one must go into every message
        # otherwise 'log will kill you'
        
        # WLOG assume p < 0.5
        # so it's better to put everything in everything's first one itself
        
        current_score = 0
        
        for i in range(M):
            high_noise_matrix[i][0] = 1
            filled_vol += 1
            # populate contrib_msg_to_score
            contrib_msg_to_score[i] = (1 - p) ** N
            current_score += msg_probs[i] * math.log2(contrib_msg_to_score[i])
    
        while filled_vol < 2 ** N:
            best_gain = -float('inf')
            best_gain_indices = (-1, -1)
            for i in range(M):
                for j in range(N + 1):
                    # search space is always M * (N + 1)
                    if high_noise_matrix[i][j] == limits[j]:
                        continue
                    new_score = compute_updated_exp_val(current_score, N, p, i, j)
                    if new_score - current_score > best_gain:
                        best_gain = new_score - current_score
                        best_gain_indices = (i, j)
            i_index, j_index = best_gain_indices
            contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p) ** (N - j_index))
            high_noise_matrix[i_index][j_index] += 1
            current_score += best_gain
            filled_vol += 1
            
        # do for all the other (actual) channel noise bit flip probabilities
        
        for p_index in range(len(pvals)):

            p = pvals[p_index]

            # print('Computing for operating prob =', p, '...')
            
            p_str = "{:.2f}".format(p)  # had to do this because 0.150000000000001 etc
            
            n_matrix = np.zeros((M, N + 1))
            filled_vol = 0
        
            # at least one must go into every message
            # otherwise 'log will kill you'
            
            # WLOG assume p < 0.5
            # so it's better to put everything in everything's first one itself
            
            current_score = 0
            
            for i in range(M):
                n_matrix[i][0] = 1
                filled_vol += 1
                # populate contrib_msg_to_score
                contrib_msg_to_score[i] = (1 - p) ** N
                current_score += msg_probs[i] * math.log2(contrib_msg_to_score[i])
        
            while filled_vol < 2 ** N:
                best_gain = -float('inf')
                best_gain_indices = (-1, -1)
                for i in range(M):
                    for j in range(N + 1):
                        # search space is always M * (N + 1)
                        if n_matrix[i][j] == limits[j]:
                            continue
                        new_score = compute_updated_exp_val(current_score, N, p, i, j)
                        if new_score - current_score > best_gain:
                            best_gain = new_score - current_score
                            best_gain_indices = (i, j)
                i_index, j_index = best_gain_indices
                contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p) ** (N - j_index))
                n_matrix[i_index][j_index] += 1
                current_score += best_gain
                filled_vol += 1
    
            
            # compute <log f> for that p with the answer to high noise configuration
            
            high_noise_exp_val = 0.0
            
            for j in range(M):
                val = 0.0
                for k in range(N + 1):
                    val += high_noise_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
                high_noise_exp_val += msg_probs[j] * math.log2(val)
                    
            actual_exp_val = 0.0
            
            for j in range(M):
                val = 0.0
                for k in range(N + 1):
                    val += n_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
                actual_exp_val += msg_probs[j] * math.log2(val)
                    
            prepn_for_worst_case.append(high_noise_exp_val)
            actual_achievable.append(actual_exp_val)
            
        for p_index in range(len(pvals)):
            p = pvals[p_index]
            hit_matrix_dict[high_noise][p] = 2 ** (prepn_for_worst_case[p_index] - actual_achievable[p_index])

    hit_matrix = pd.DataFrame(hit_matrix_dict)
    ax = sns.heatmap(hit_matrix, vmin=0.4, vmax=1, xticklabels = 2, yticklabels = 2)
    plt.title('Heatmap of performance ratio for t = ' + str(t))
    plt.ylabel('Operating p')
    plt.xlabel('P optimized for')
    plt.savefig('Plots/RobVsRes/heatmap-' + str(t) + '.svg', format='svg', dpi = 1200) 
    plt.figure()
            
# ax = sns.heatmap(hit_matrix, xticklabels=2, vmin = 0, vmax = 1)
# plt.title('Heatmap of 2**performance ratio (optimized for / operating)')
# plt.ylabel('P optimized for')
# plt.xlabel('Operating p')
# plt.savefig('Plots/heatmap.svg', format='svg', dpi = 1200)   

print(hit_matrix_dict) 