# NOTE: code only works for two sources

import numpy as np
import scipy.misc
#import sys
#import matplotlib.pyplot as plt

M = 100
t = 1

msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
#msg_probs = np.random.random(M)
msg_probs = list(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)

N = 15
#p = 0.1

limits = [scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)]

for p1 in np.arange(0.0, 0.51, 0.01):
    p1 = 0.15
    print('Checking against p1 =', p1, '...')
    for p2 in np.arange(0.0, 0.51, 0.01):
        p2 = 0.12
        print('p2 =', p2)
        if p1 == p2:
            continue
        filled_vol = 0
        total_vol = 2 ** N
        
        source1_error_prob = p1
        source2_error_prob = p2
        
        source_error_probs = [source1_error_prob, source2_error_prob]
        no_sources = len(source_error_probs)
        
        n_matrix = np.zeros((M, N + 1, no_sources))
        
        gains_dict = {}
        for i in range(M):
            for j in range(N + 1):
                for k in range(no_sources):
                    gain = msg_probs[i] * (source_error_probs[k] ** j) * ((1 - source_error_probs[k]) ** (N - j))
                    if gain not in gains_dict:
                        gains_dict[gain] = []
                    gains_dict[gain].append((i, j, k))
                
        gains_sorted = list(gains_dict.keys())
        gains_sorted.sort(reverse = True)
        
        filled_vol = 0
        gain_index = 0
        
        while gain_index < M * N * no_sources and filled_vol < total_vol:
            
            gain = gains_sorted[gain_index]
            
            for i, j, source in gains_dict[gain]:
                
                # first, check if the message has already been assigned to the other channel
                # if not, do the usual thing as for with one source
                if not np.any(n_matrix[i, :, 1 - source] > 0):
                    if filled_vol + limits[j] <= total_vol:
                        # assign all of them to this one
                        n_matrix[i][j][source] = limits[j]
                        filled_vol += limits[j]
                    else:
                        n_matrix[i][j][source] = total_vol - filled_vol
                        filled_vol = total_vol
                else:
                    # this is the tricky part
                    # you need to check whether to it's better sending message through channel 0 or 1
                    # for this, you need to check maximum probability of reconstruction while sending
                    # these many messages through channel 0 or 1
                    msg_i_seqs_count = np.sum(n_matrix[i, :, 1 - source]) + 1   # no. of msgs decoding to msg i
                    # plus one because of the extra message we're trying to assign
                    
                    # decide which channel to assign these many messages to
                    
                    ch_zero_gain = 0.0
                    ch_zero_gains = [(source_error_probs[0] ** t) * ((1 - source_error_probs[0]) ** (N - t)) for t in range(N)]
                    j_indices = list(range(N))
                    sorted_j_indices = [x for _, x in sorted(zip(ch_zero_gains, j_indices), reverse = True)]
                    
                    t_index = 0
                    remaining_msg_count = msg_i_seqs_count
                    while t_index < N and remaining_msg_count > 0:
                        if remaining_msg_count >= limits[sorted_j_indices[t_index]]:
                            ch_zero_gain += limits[sorted_j_indices[t_index]] * ch_zero_gains[sorted_j_indices[t_index]]
                            remaining_msg_count -= limits[sorted_j_indices[t_index]]
                        else:
                            ch_zero_gain += remaining_msg_count * ch_zero_gains[sorted_j_indices[t_index]]
                            remaining_msg_count = 0
                        t_index += 1
                        
                    # do the same thing for the second channel
                    
                    ch_one_gain = 0.0
                    ch_one_gains = [(source_error_probs[1] ** t) * ((1 - source_error_probs[1]) ** (N - t)) for t in range(N)]
                    j_indices = list(range(N))
                    sorted_j_indices = [x for _, x in sorted(zip(ch_one_gains, j_indices), reverse = True)]
                    
                    t_index = 0
                    remaining_msg_count = msg_i_seqs_count
                    while t_index < N and remaining_msg_count > 0:
                        if remaining_msg_count >= limits[sorted_j_indices[t_index]]:
                            ch_one_gain += limits[sorted_j_indices[t_index]] * ch_one_gains[sorted_j_indices[t_index]]
                            remaining_msg_count -= limits[sorted_j_indices[t_index]]
                        else:
                            ch_one_gain += remaining_msg_count * ch_one_gains[sorted_j_indices[t_index]]
                            remaining_msg_count = 0
                        t_index += 1
                        
                    print('ch0, ch1 =', ch_zero_gain, ch_one_gain)
                        
                    if ch_one_gain >= ch_zero_gain:
                        if source == 0:
                            # we were supposed to assign to channel zero
                            # but turned out same message was assigned to channel one
                            # now we're saying we gain more overall if we assign the sequence to channel one
                            # so this one was just a case of short-term gain
                            continue
                    else:
                        if source == 1:
                            # (same reasoning as above)
                            continue
                    n_matrix[i][j][source] += 1
                    
            gain_index += 1
            
        first_channel_seqs = np.sum(n_matrix[:, :, 0])
        second_channel_seqs = np.sum(n_matrix[:, :, 1])
        
        if first_channel_seqs != 0 and second_channel_seqs != 0:
            print('Messages sent through both channels for p1, p2 =', p1, ',', p2)
        
        if p1 > p2:
            if first_channel_seqs > second_channel_seqs:
                # there was more error for channel 1
                # still messages were sent through it
                # so something's wrong!
                print('Outlier detected for p1, p2 =', p1, ',', p2)
                print('No of sequences assigned to the first channel =', first_channel_seqs)
                print('No of sequences assigned to the second channel =', second_channel_seqs)
        else:
            if second_channel_seqs > first_channel_seqs:
                # there was more error for channel 2
                # still messages were sent through it
                # so something's wrong!
                print('Outlier detected for p1, p2 =', p1, ',', p2)
                print('No of sequences assigned to the first channel =', first_channel_seqs)
                print('No of sequences assigned to the second channel =', second_channel_seqs)
        if filled_vol != total_vol:
            print('Not all sequences assigned for p1, p2 =', p1, ',', p2)
            print('Total #sequences =', total_vol)
            print('Assigned sequences =', filled_vol)   
            print('Fractional volume filled =', filled_vol / total_vol)
            
        break
    break
            