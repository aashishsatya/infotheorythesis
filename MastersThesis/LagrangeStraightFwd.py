import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.special
import scipy.stats as stats

M = 100
t = 2
msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)
msg_probs = np.array(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

N = 15
lambda_epsilon = 0.001  # epsilon for binary searching lambda
delta_epsilon = 0.001   # epsilon for binary searching delta
vol_epsilon = 0.001 # epsilon for volume

exp_vals = []
two_power_exp_vals = []
pvals = list(np.arange(0.03, 0.51, 0.01))

for p in pvals:
    
#    print('Computing for p =', p)

    lambda_high = 10000.0
    lambda_low = 0
    
    while abs(lambda_low - lambda_high) >= lambda_epsilon:
        
        lambda_val = (lambda_high + lambda_low) / 2.0 
        
        delta_js_log_f = []   # delta j's
        
        for j in range(M):
        
            reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
    
            # find out the value of delta_j for that particular lambda using bisection search
        
            high = N
            low = 0.0
            delta = 0.0
            
            while abs(high - low) >= delta_epsilon:    
                
                delta = (high + low) / 2.0
            
                exponent = (delta - N / 2.0)**2 / (N / 2)
                exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                
                num = math.e ** exponent
                
                denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                denom *= 0.5
                
                ratio = num / denom
                
                if ratio > reqd_ratio:
                    # need smaller delta
                    low = delta
                else:
                    high = delta               
            delta_js_log_f.append(delta)
            
        # find out the U value for this set of delta j's
        total_vol = 0.0
        for j in range(M):
            vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
            vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
            vol *= 0.5
            total_vol += vol
            
        if abs(total_vol - 1) < vol_epsilon:
            break
        
        if total_vol > 1:
            # move farther to the right
            lambda_low = lambda_val
        else:
            lambda_high = lambda_val
            
    exp_val = 0.0
    for i in range(M):
        f_i = math.erf((delta_js_log_f[i] - N * p) / math.sqrt(2 * N * p * (1 - p)))
        f_i += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
        f_i *= 0.5
        exp_val += msg_probs[i] * math.log2(f_i)
        
    exp_vals.append(exp_val)
    two_power_exp_vals.append(2 ** exp_val)
    
plt.plot(pvals, exp_vals)
plt.title('p vs <log f>_g using Lagrange Multipliers')
plt.xlabel('Probability of bit flip (p)')
plt.ylabel('<log f>_g')

plt.figure()
plt.plot(pvals, two_power_exp_vals)
plt.title('p vs 2 ** <log f>_g using Lagrange Multipliers')
plt.xlabel('Probability of bit flip (p)')
plt.ylabel('2 ** <log f>_g')