import numpy as np
import math
import matplotlib.pyplot as plt
from decimal import Decimal
import scipy.misc

M = 100
#tvals = list(np.arange(1, 3.1, 0.25))
tvals = [3] # for testing purposes

N = 15
p = 0.15
L = 2 ** N # for now, say
total_vol = L

lambda_epsilon = 0.001  # epsilon for binary searching lambda
delta_epsilon = 0.001   # epsilon for binary searching delta
vol_epsilon = 0.001 # epsilon for volume

for t in tvals:
    
    print('Computing for t =', t, '...')
    msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
#    msg_probs = np.array([1.0] * M)   # for sanity checks
    msg_probs = list(msg_probs)
    msg_probs.sort(reverse = True)
    msg_probs = np.array(msg_probs)
    sum_probs = sum(msg_probs)
    msg_probs /= sum_probs
    
#    print(msg_probs)
    
    lambda_high = 10000.0
    lambda_low = 0
    
    while abs(lambda_low - lambda_high) >= lambda_epsilon:
        
        lambda_val = (lambda_high + lambda_low) / 2.0 
        
        delta_js_log_f = []   # delta j's
        
        for j in range(M):
        
            reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
    
            # find out the value of delta_j for that particular lambda using bisection search
        
            high = N
            low = 0.0
            delta = 0.0
            
            while abs(high - low) >= delta_epsilon:    
                
                delta = (high + low) / 2.0
            
                exponent = (delta - N / 2.0)**2 / (N / 2)
                exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                
                num = math.e ** exponent
                
                denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                denom *= 0.5
                
                ratio = num / denom
                
                if ratio > reqd_ratio:
                    # need smaller delta
                    low = delta
                else:
                    high = delta               
            delta_js_log_f.append(delta)
            
        # find out the U value for this set of delta j's
        total_vol = 0.0
        for j in range(M):
            vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
            vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
            vol *= 0.5
            total_vol += vol
            
        if abs(total_vol - 1) < vol_epsilon:
            break
        
        if total_vol > 1:
            # move farther to the right
            lambda_low = lambda_val
        else:
            lambda_high = lambda_val
            
    print(delta_js_log_f)
            
    # compute bit flip vs recovery probability
    
    bit_flip_probs = [1]    # 1 because prob of zero bit flip
    delta_js_log_f.sort()
    x_axis = [0] + delta_js_log_f
    
    prob_running_sum = 0
    for i in range(M):
#        prob_running_sum += msg_probs[(M - 1) - i]
        bit_flip_probs.append(1 - prob_running_sum)
        prob_running_sum += msg_probs[(M - 1) - i]
    bit_flip_probs.append(0)
    x_axis.append(N)
        
    plt.step(x_axis, bit_flip_probs)
plt.title('#bits flipped vs prob reconstrn for t = ' + str(t))
plt.xlabel('#bits flipped')
plt.ylabel('Prob of correct reconstrn')
plt.legend(['t = ' + t_val for t_val in list(map(str, tvals))])
    
    # for putting the legend outside the plot
#    ax = plt.subplot(111)
#    box = ax.get_position()
#    ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
#    legend_x = 1
#    legend_y = 0.5
#    legend_x = 1
#    legend_y = 0.5    
#    legend_list = ['g_' + str(msg_index) + ' = ' + '%.3e' % Decimal(str(msg_probs[msg_index])) for msg_index in msg_indices]
#    plt.legend(legend_list, loc='center left', bbox_to_anchor=(legend_x, legend_y))
#    plt.savefig('Plots/BitsFlippedvsProbForMsgt' + str(t) +'.svg', format='svg', dpi=1200)
#    plt.figure()
    
