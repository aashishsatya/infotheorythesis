import numpy as np
import scipy.misc
#import sys
import matplotlib.pyplot as plt
import math

def compute_exp_val(n_matrix):
    
    """
    Returns <log f>_g as of the entries in the n_matrix
    """
    
    current_score = 0
    for i in range(M):
        prob = 0
        for j in range(N):
            prob += n_matrix[i][j] * (p ** j) * ((1 - p) ** (N - j))
        current_score += msg_probs[i] * math.log2(prob)
    return current_score
    
    

M = 100
t = 1
msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
#msg_probs = np.random.random(M)
msg_probs = list(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)

N = 10
p = 0.2
L = 500 # for now, say

limits = [scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)]

n_matrix = np.zeros((M, N + 1))

filled_vol = 0
total_vol = L

# at least one must go into everything
# otherwise 'log will kill you'

# WLOG assume p < 0.5
# so its better to put everything in everything's first one itself

for i in range(M):
    n_matrix[i][0] = 1
    filled_vol += 1
   
current_score = compute_exp_val(n_matrix)

while filled_vol < L:
    best_gain = -float('inf')
    best_gain_indices = (-1, -1)
    for i in range(M):
        for j in range(N + 1):
            # search space is always M * (N + 1)
            if n_matrix[i][j] == limits[j]:
                continue
            n_matrix[i][j] += 1
            new_score = compute_exp_val(n_matrix)
            n_matrix[i][j] -= 1
            if new_score - current_score > best_gain:
                best_gain = new_score - current_score
                best_gain_indices = (i, j)
    print('Best gain indices =', best_gain_indices)
    i_index, j_index = best_gain_indices
    n_matrix[i_index][j_index] += 1
    current_score = compute_exp_val(n_matrix)
    filled_vol += 1
    print('Filled vol =', filled_vol)

#gains_dict = {}
#for i in range(M):
#    for j in range(N + 1):
#        gain = msg_probs[i] * (p ** j) * ((1 - p) ** (N - j))
#        if gain not in gains_dict:
#            gains_dict[gain] = []
#        gains_dict[gain].append((i, j))
#        
#gains_sorted = list(gains_dict.keys())
#gains_sorted.sort(reverse = True)
#
##print(gains_dict)
#
## limit will only depend on the j value
#
#filled_vol = 0
#gain_index = 0
#while gain_index < M * N and filled_vol < total_vol:
#    gain = gains_sorted[gain_index]
#    for i, j in gains_dict[gain]:
#        if filled_vol + limits[j] <= total_vol:
#            # assign all of them to this one
#            n_matrix[i][j] = limits[j]
#            filled_vol += limits[j]
#        else:
#            n_matrix[i][j] = total_vol - filled_vol
#            filled_vol = total_vol
#    gain_index += 1
    
print('Fractional volume filled =', filled_vol / total_vol)
print('2**N =', 2 ** N)
print('Total #decodable sequences =', int(np.sum(n_matrix)))
#print(limits)
#print(np.sum(n_matrix, axis = 0))
prob_correct_reconstrn = np.sum(n_matrix, axis = 0) / (M * np.array(limits))
#print(prob_correct_reconstrn)
plt.plot(list(range(0, N + 1)), prob_correct_reconstrn)
plt.title('#bits flipped vs probability of reconstruction')
plt.xlabel('#bits flipped')
plt.ylabel('Probability of correct reconstruction')
plt.show()

print('<log f>_g =', current_score)

            