import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.misc

def compute_updated_exp_val(current_score, N, p, i, j):
    
    """
    Returns <log f>_g as of the entries in the n_matrix
    """
    
    global contrib_msg_to_score
    
    # what has got invalidated is the component resulting from the ith message
    
    updated_score = current_score - msg_probs[i] * math.log2(contrib_msg_to_score[i])
    updated_score += msg_probs[i] * math.log2(contrib_msg_to_score[i] + (p ** j) * ((1 - p)) ** (N - j))
    return updated_score

M = 100
pvals = list(np.arange(0.05, 0.46, 0.03))
high_noises = [0.10]

#tvals = list(np.arange(0.5, 3.1, 0.5))
tvals = [2]
#tvals = [3] # for testing

N = 10
L = 2 ** N # for now, say

lambda_epsilon = 0.001  # epsilon for binary searching lambda
delta_epsilon = 0.001   # epsilon for binary searching delta
vol_epsilon = 0.001 # epsilon for volume

limits = np.array([scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)])
contrib_msg_to_score = [0] * M # contribution of each message to the score

for t in tvals:
    
    for high_noise in high_noises:
    
        contrib_msg_to_score = [0] * M # contribution of each message to the score
        
        msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
        #    msg_probs = np.array([1.0] * M)   # for sanity checks
        msg_probs = list(msg_probs)
        msg_probs.sort(reverse = True)
        msg_probs = np.array(msg_probs)
        sum_probs = sum(msg_probs)
        msg_probs /= sum_probs
        
        prepn_for_worst_case = []
        actual_achievable = []
        
        print('Computing for t =', t, '...')
        
        # compute for high noise regime
        
        p = high_noise  # for now
        high_noise_str = "{:.2f}".format(high_noise)  # had to do this because 0.150000000000001 etc
            
        high_noise_matrix = np.zeros((M, N + 1))
        filled_vol = 0
    
        # at least one must go into every message
        # otherwise 'log will kill you'
        
        # WLOG assume p < 0.5
        # so it's better to put everything in everything's first one itself
        
        current_score = 0
        
        for i in range(M):
            high_noise_matrix[i][0] = 1
            filled_vol += 1
            # populate contrib_msg_to_score
            contrib_msg_to_score[i] = (1 - p) ** N
            current_score += msg_probs[i] * math.log2(contrib_msg_to_score[i])
    
        while filled_vol < L:
            best_gain = -float('inf')
            best_gain_indices = (-1, -1)
            for i in range(M):
                for j in range(N + 1):
                    # search space is always M * (N + 1)
                    if high_noise_matrix[i][j] == limits[j]:
                        continue
                    new_score = compute_updated_exp_val(current_score, N, p, i, j)
                    if new_score - current_score > best_gain:
                        best_gain = new_score - current_score
                        best_gain_indices = (i, j)
            i_index, j_index = best_gain_indices
            contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p) ** (N - j_index))
            high_noise_matrix[i_index][j_index] += 1
            current_score += best_gain
            filled_vol += 1
            
        # do for all the other (actual) channel noise bit flip probabilities
        
        for p in pvals:
            
            p_str = "{:.2f}".format(p)  # had to do this because 0.150000000000001 etc
            
            n_matrix = np.zeros((M, N + 1))
            filled_vol = 0
        
            # at least one must go into every message
            # otherwise 'log will kill you'
            
            # WLOG assume p < 0.5
            # so it's better to put everything in everything's first one itself
            
            current_score = 0
            
            for i in range(M):
                n_matrix[i][0] = 1
                filled_vol += 1
                # populate contrib_msg_to_score
                contrib_msg_to_score[i] = (1 - p) ** N
                current_score += msg_probs[i] * math.log2(contrib_msg_to_score[i])
        
            while filled_vol < L:
                best_gain = -float('inf')
                best_gain_indices = (-1, -1)
                for i in range(M):
                    for j in range(N + 1):
                        # search space is always M * (N + 1)
                        if n_matrix[i][j] == limits[j]:
                            continue
                        new_score = compute_updated_exp_val(current_score, N, p, i, j)
                        if new_score - current_score > best_gain:
                            best_gain = new_score - current_score
                            best_gain_indices = (i, j)
                i_index, j_index = best_gain_indices
                contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p) ** (N - j_index))
                n_matrix[i_index][j_index] += 1
                current_score += best_gain
                filled_vol += 1
    
            
            # compute <log f> for that p with the answer to high noise configuration
            
            high_noise_exp_val = 0.0
            
            for j in range(M):
                val = 0.0
                for k in range(N + 1):
                    val += high_noise_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
                high_noise_exp_val += msg_probs[j] * math.log2(val)
                    
            actual_exp_val = 0.0
            
            for j in range(M):
                val = 0.0
                for k in range(N + 1):
                    val += n_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
                actual_exp_val += msg_probs[j] * math.log2(val)
                    
            prepn_for_worst_case.append(high_noise_exp_val)
            actual_achievable.append(actual_exp_val)
            
        plt.plot(pvals, prepn_for_worst_case)
        plt.plot(pvals, actual_achievable)
        
        # code for individual plots below      
        
        plt.title('Worst vs achievable; (M, N, t, noise p) = ' + str(tuple([M, N, t, float(high_noise_str)])))
        plt.xlabel('Actual bit flip probability')
        plt.ylabel('<log f>')
        plt.legend(['Prepn for worst case', 'Actual achievable'])
        plt.savefig('Plots/PrepnForWorstFixedT/Greedy/ylim-' + high_noise_str + '-' + str(t) +'.svg', format='svg', dpi=1200)
        plt.show()
        plt.close()
        plt.figure()
        
        plt.plot(pvals, [2 ** val for val in np.array(prepn_for_worst_case) - np.array(actual_achievable)])
        plt.title('Hit for (M, N, t, noise p) = ' + str(tuple([M, N, t, float(high_noise_str)])))
        plt.xlabel('Actual bit flip probability')
        plt.ylabel('2 ** (<log>_prepn - <log>_actual)')
        plt.ylim((0.5, 1.05))
        plt.savefig('Plots/PrepnForWorstFixedT/Greedy/ylim-diff-' + high_noise_str + '-' + str(t) +'.svg', format='svg', dpi=1200)
    #    plt.legend(['Prepn for worst case', 'Actual achievable'])
        plt.show()
        plt.figure()
#    plt.plot(pvals, [2 ** val for val in x])
    
# code for multiple graphs in one plot
    
#plt.title('#bits flipped vs prob corrn; (M, N, t) = ' + str(tuple([M, N, t])))
#plt.xlabel('#bits flipped')
#plt.ylabel('Prob(correct reconstrn | #bits flipped)')
#
## for putting the legend outside the plot        
#ax = plt.subplot(111)
#box = ax.get_position()
#ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
#legend_x = 1
#legend_y = 0.5
#legend_x = 1
#legend_y = 0.5    
#legend_list = ['p = ' + "{:.2f}".format(p) for p in pvals]
#plt.legend(legend_list, loc='center left', bbox_to_anchor=(legend_x, legend_y))
#plt.savefig('Plots/BitsFlippedFixedT/Lagrange/P.svg', format='svg', dpi=1200)
#plt.show()