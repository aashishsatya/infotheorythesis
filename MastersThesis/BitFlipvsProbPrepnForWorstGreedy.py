import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.misc

def compute_updated_exp_val(current_score, N, p, i, j):
    
    """
    Returns <log f>_g as of the entries in the n_matrix
    """
    
    global contrib_msg_to_score
    
    # what has got invalidated is the component resulting from the ith message
    
    updated_score = current_score - msg_probs[i] * math.log2(contrib_msg_to_score[i])
    updated_score += msg_probs[i] * math.log2(contrib_msg_to_score[i] + (p ** j) * ((1 - p)) ** (N - j))
    return updated_score

pvals = list(np.arange(0.05, 0.46, 0.10))
#Mvals = [100, 150, 200]
Mvals = [150, 200]
#Nvals = list(range(10, 31, 5))
Nvals = [10]
high_noise = 0.45
tvals = list(np.arange(0.5, 3.1, 0.5))
#tvals = [3] # for testing

for M in Mvals:
    print('Computing for M =', M, '...')
    for N in Nvals:
        print('Computing for N =', N, '...')
        L = 2 ** N
        for t in tvals:
            
            limits = np.array([scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)])
            t_str = str(t)
            contrib_msg_to_score = [0] * M # contribution of each message to the score
            
            msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
            #    msg_probs = np.array([1.0] * M)   # for sanity checks
            msg_probs = list(msg_probs)
            msg_probs.sort(reverse = True)
            msg_probs = np.array(msg_probs)
            sum_probs = sum(msg_probs)
            msg_probs /= sum_probs
            
            prepn_for_worst_case = []
            actual_achievable = []
            
            print('Computing for t =', t, '...')
            
            # compute for high noise regime
            
            p = high_noise
            
            p_str = "{:.2f}".format(p)  # had to do this because 0.150000000000001 etc
                
            high_noise_matrix = np.zeros((M, N + 1))
            filled_vol = 0
        
            # at least one must go into every message
            # otherwise 'log will kill you'
            
            # WLOG assume p < 0.5
            # so it's better to put everything in everything's first one itself
            
            current_score = 0
            
            for i in range(M):
                high_noise_matrix[i][0] = 1
                filled_vol += 1
                # populate contrib_msg_to_score
                contrib_msg_to_score[i] = (1 - p) ** N
                current_score += msg_probs[i] * math.log2(contrib_msg_to_score[i])
        
            while filled_vol < L:
                best_gain = -float('inf')
                best_gain_indices = (-1, -1)
                for i in range(M):
                    for j in range(N + 1):
                        # search space is always M * (N + 1)
                        if high_noise_matrix[i][j] == limits[j]:
                            continue
                        new_score = compute_updated_exp_val(current_score, N, p, i, j)
                        if new_score - current_score > best_gain:
                            best_gain = new_score - current_score
                            best_gain_indices = (i, j)
                i_index, j_index = best_gain_indices
                contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p) ** (N - j_index))
                high_noise_matrix[i_index][j_index] += 1
                current_score += best_gain
                filled_vol += 1
                
            high_noise_bit_flip_prob = np.sum(high_noise_matrix, axis = 0) / (M * np.array(limits))
                
            # do for all the other (actual) channel noise bit flip probabilities
            
            for p in pvals:
                
                p_str = "{:.2f}".format(p)  # had to do this because 0.150000000000001 etc
                
                n_matrix = np.zeros((M, N + 1))
                filled_vol = 0
            
                # at least one must go into every message
                # otherwise 'log will kill you'
                
                # WLOG assume p < 0.5
                # so it's better to put everything in everything's first one itself
                
                current_score = 0
                
                for i in range(M):
                    n_matrix[i][0] = 1
                    filled_vol += 1
                    # populate contrib_msg_to_score
                    contrib_msg_to_score[i] = (1 - p) ** N
                    current_score += msg_probs[i] * math.log2(contrib_msg_to_score[i])
            
                while filled_vol < L:
                    best_gain = -float('inf')
                    best_gain_indices = (-1, -1)
                    for i in range(M):
                        for j in range(N + 1):
                            # search space is always M * (N + 1)
                            if n_matrix[i][j] == limits[j]:
                                continue
                            new_score = compute_updated_exp_val(current_score, N, p, i, j)
                            if new_score - current_score > best_gain:
                                best_gain = new_score - current_score
                                best_gain_indices = (i, j)
                    i_index, j_index = best_gain_indices
                    contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p) ** (N - j_index))
                    n_matrix[i_index][j_index] += 1
                    current_score += best_gain
                    filled_vol += 1
                    
                p_bit_flip_prob = np.sum(n_matrix, axis = 0) / (M * np.array(limits))
                
                plt.plot(list(range(N + 1)), high_noise_bit_flip_prob)
                plt.plot(list(range(N + 1)), p_bit_flip_prob)
                
                plt.title('Worst vs achievable; (M, N, t, noise p) = ' + str(tuple([M, N, t, float(p_str)])))
                plt.xlabel('#bit flips')
                plt.ylabel('P(reconstrn|# bit flip)')
                plt.legend(['Prepn for worst case', 'Actual achievable'])
                filename = str(M) + '-' + str(N) + '-' + t_str + '-' + p_str
                plt.savefig('Plots/BitFlipvsProbPrepnForWorst/Greedy/' + filename +'.svg', format='svg', dpi=1200)
        #        plt.show()
                plt.close()
                plt.figure()