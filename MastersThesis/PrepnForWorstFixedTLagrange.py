import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.misc

M = 100
pvals = list(np.arange(0.05, 0.46, 0.01))
high_noise = 0.45

tvals = list(np.arange(0.5, 3.1, 0.5))
#tvals = [3] # for testing

N = 10
L = 2 ** N # for now, say

lambda_epsilon = 0.001  # epsilon for binary searching lambda
delta_epsilon = 0.001   # epsilon for binary searching delta
vol_epsilon = 0.001 # epsilon for volume

limits = np.array([scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)])

for t in tvals:
    
    msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
    #    msg_probs = np.array([1.0] * M)   # for sanity checks
    msg_probs = list(msg_probs)
    msg_probs.sort(reverse = True)
    msg_probs = np.array(msg_probs)
    sum_probs = sum(msg_probs)
    msg_probs /= sum_probs
    
    prepn_for_worst_case = []
    actual_achievable = []
    
    print('Computing for t =', t, '...')
    
    # compute for high noise regime
    
    p = high_noise
    
    p_str = "{:.2f}".format(p)  # had to do this because 0.150000000000001 etc
        
    lambda_high = 10000.0
    lambda_low = 0
    
    while abs(lambda_low - lambda_high) >= lambda_epsilon:
        
        lambda_val = (lambda_high + lambda_low) / 2.0 
        
        delta_js_log_f = []   # delta j's
        
        for j in range(M):
        
            reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
    
            # find out the value of delta_j for that particular lambda using bisection search
        
            high = N
            low = 0.0
            delta = 0.0
            
            while abs(high - low) >= delta_epsilon:    
                
                delta = (high + low) / 2.0
            
                exponent = (delta - N / 2.0)**2 / (N / 2)
                exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                
                num = math.e ** exponent
                
                denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                denom *= 0.5
                
                ratio = num / denom
                
                if ratio > reqd_ratio:
                    # need smaller delta
                    low = delta
                else:
                    high = delta               
            delta_js_log_f.append(delta)
            
        # find out the U value for this set of delta j's
        total_vol = 0.0
        for j in range(M):
            vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
            vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
            vol *= 0.5
            total_vol += vol
            
        if abs(total_vol - 1) < vol_epsilon:
            break
        
        if total_vol > 1:
            # move farther to the right
            lambda_low = lambda_val
        else:
            lambda_high = lambda_val
            
    
    delta_js_log_f.sort(reverse = True)
    high_noise_approx_matrix = np.zeros((M, N + 1))
    for i in range(M):
        for j in range(N):
            if delta_js_log_f[i] >= j:
                high_noise_approx_matrix[i][j] = limits[j]
            else:
                break
    
    for p in pvals:
        
        p_str = "{:.2f}".format(p)  # had to do this because 0.150000000000001 etc
        
#        print('Computing for p =', p, '...')
        
        lambda_high = 10000.0
        lambda_low = 0
        
        while abs(lambda_low - lambda_high) >= lambda_epsilon:
            
            lambda_val = (lambda_high + lambda_low) / 2.0 
            
            delta_js_log_f = []   # delta j's
            
            for j in range(M):
            
                reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
        
                # find out the value of delta_j for that particular lambda using bisection search
            
                high = N
                low = 0.0
                delta = 0.0
                
                while abs(high - low) >= delta_epsilon:    
                    
                    delta = (high + low) / 2.0
                
                    exponent = (delta - N / 2.0)**2 / (N / 2)
                    exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                    
                    num = math.e ** exponent
                    
                    denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                    denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                    denom *= 0.5
                    
                    ratio = num / denom
                    
                    if ratio > reqd_ratio:
                        # need smaller delta
                        low = delta
                    else:
                        high = delta               
                delta_js_log_f.append(delta)
                
            # find out the U value for this set of delta j's
            total_vol = 0.0
            for j in range(M):
                vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                vol *= 0.5
                total_vol += vol
                
            if abs(total_vol - 1) < vol_epsilon:
                break
            
            if total_vol > 1:
                # move farther to the right
                lambda_low = lambda_val
            else:
                lambda_high = lambda_val
                
        
        delta_js_log_f.sort(reverse = True)
        approx_matrix = np.zeros((M, N + 1))
        for i in range(M):
            for j in range(N):
                if delta_js_log_f[i] >= j:
                    approx_matrix[i][j] = limits[j]
                else:
                    break
        
        # compute <log f> for that p with the answer to high noise configuration
        
        high_noise_exp_val = 0.0
        
        for j in range(M):
            val = 0.0
            for k in range(N + 1):
                val += high_noise_approx_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
            high_noise_exp_val += msg_probs[j] * math.log2(val)
                
        actual_exp_val = 0.0
        
        for j in range(M):
            val = 0.0
            for k in range(N + 1):
                val += approx_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
            actual_exp_val += msg_probs[j] * math.log2(val)
                
        prepn_for_worst_case.append(high_noise_exp_val)
        actual_achievable.append(actual_exp_val)
        
    plt.plot(pvals, prepn_for_worst_case)
    plt.plot(pvals, actual_achievable)
    
    # code for individual plots below      
    
    plt.title('Preparing for the worst vs achievable; (M, N, t, noise p) = ' + str(tuple([M, N, t, float(p_str)])))
    plt.xlabel('Actual bit flip probability')
    plt.ylabel('<log f>')
    plt.legend(['Prepn for worst case', 'Actual achievable'])
    plt.savefig('Plots/PrepnForWorstFixedT/Lagrange/' + str(t) +'.svg', format='svg', dpi=1200)
    plt.show()
    plt.close()
    plt.figure()
    
# code for multiple graphs in one plot
    
#plt.title('#bits flipped vs prob corrn; (M, N, t) = ' + str(tuple([M, N, t])))
#plt.xlabel('#bits flipped')
#plt.ylabel('Prob(correct reconstrn | #bits flipped)')
#
## for putting the legend outside the plot        
#ax = plt.subplot(111)
#box = ax.get_position()
#ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
#legend_x = 1
#legend_y = 0.5
#legend_x = 1
#legend_y = 0.5    
#legend_list = ['p = ' + "{:.2f}".format(p) for p in pvals]
#plt.legend(legend_list, loc='center left', bbox_to_anchor=(legend_x, legend_y))
#plt.savefig('Plots/BitsFlippedFixedT/Lagrange/P.svg', format='svg', dpi=1200)
#plt.show()