import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.misc

def compute_updated_exp_val(current_score, N, p, i, j):
    
    """
    Returns <log f>_g as of the entries in the n_matrix
    """
    
    global contrib_msg_to_score
    
    # what has got invalidated is the component resulting from the ith message
    
    updated_score = current_score - msg_probs[i] * math.log2(contrib_msg_to_score[i])
    updated_score += msg_probs[i] * math.log2(contrib_msg_to_score[i] + (p ** j) * ((1 - p)) ** (N - j))
    return updated_score

M = 100
tvals = list(np.arange(2.5, 3.1, 0.5))
#tvals = [3] # for testing purposes

N = 13
p = 0.15
L = 2 ** N # for now, say
total_vol = L

lambda_epsilon = 0.001  # epsilon for binary searching lambda
delta_epsilon = 0.001   # epsilon for binary searching delta
vol_epsilon = 0.001 # epsilon for volume

limits = np.array([scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)])
contrib_msg_to_score = [0] * M # contribution of each message to the score

for t in tvals:
    
    p_str = "{:.2f}".format(p)  # had to do this because 0.150000000000001 etc
    
    print('Computing for t =', t, '...')
    msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
#    msg_probs = np.array([1.0] * M)   # for sanity checks
    msg_probs = list(msg_probs)
    msg_probs.sort(reverse = True)
    msg_probs = np.array(msg_probs)
    sum_probs = sum(msg_probs)
    msg_probs /= sum_probs
    
    lambda_high = 10000.0
    lambda_low = 0
    
    while abs(lambda_low - lambda_high) >= lambda_epsilon:
        
        lambda_val = (lambda_high + lambda_low) / 2.0 
        
        delta_js_log_f = []   # delta j's
        
        for j in range(M):
        
            reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
    
            # find out the value of delta_j for that particular lambda using bisection search
        
            high = N
            low = 0.0
            delta = 0.0
            
            while abs(high - low) >= delta_epsilon:    
                
                delta = (high + low) / 2.0
            
                exponent = (delta - N / 2.0)**2 / (N / 2)
                exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                
                num = math.e ** exponent
                
                denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                denom *= 0.5
                
                ratio = num / denom
                
                if ratio > reqd_ratio:
                    # need smaller delta
                    low = delta
                else:
                    high = delta               
            delta_js_log_f.append(delta)
            
        # find out the U value for this set of delta j's
        total_vol = 0.0
        for j in range(M):
            vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
            vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
            vol *= 0.5
            total_vol += vol
            
        if abs(total_vol - 1) < vol_epsilon:
            break
        
        if total_vol > 1:
            # move farther to the right
            lambda_low = lambda_val
        else:
            lambda_high = lambda_val
            
    # compute bit flip vs recovery probability
    
    bit_flip_probs = [1]    # 1 because prob of zero bit flip
    delta_js_log_f.sort()
    x_axis = [0] + delta_js_log_f
    
    prob_running_sum = 0
    for i in range(M):
#        prob_running_sum += msg_probs[(M - 1) - i]
        bit_flip_probs.append(1 - prob_running_sum)
        prob_running_sum += msg_probs[(M - 1) - i]
    bit_flip_probs.append(0)
    x_axis.append(N)
        
#    plt.step(x_axis, bit_flip_probs)
    
    plt.title('#bits flipped vs prob corrn for (M, N, t, p) = ' + str(tuple([M, N, t, float(p_str)])))
    plt.xlabel('#bits flipped')
    plt.ylabel('Prob of correct reconstrn')
    
    # now plot for Greedy
    
    n_matrix = np.zeros((M, N + 1))
    filled_vol = 0

    # at least one must go into every message
    # otherwise 'log will kill you'
    
    # WLOG assume p < 0.5
    # so it's better to put everything in everything's first one itself
    
    current_score = 0
    
    for i in range(M):
        n_matrix[i][0] = 1
        filled_vol += 1
        # populate contrib_msg_to_score
        contrib_msg_to_score[i] = (1 - p) ** N
        current_score += msg_probs[i] * math.log2(contrib_msg_to_score[i])

    while filled_vol < L:
        best_gain = -float('inf')
        best_gain_indices = (-1, -1)
        for i in range(M):
            for j in range(N + 1):
                # search space is always M * (N + 1)
                if n_matrix[i][j] == limits[j]:
                    continue
                new_score = compute_updated_exp_val(current_score, N, p, i, j)
                if new_score - current_score > best_gain:
                    best_gain = new_score - current_score
                    best_gain_indices = (i, j)
        i_index, j_index = best_gain_indices
        contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p) ** (N - j_index))
        n_matrix[i_index][j_index] += 1
        current_score += best_gain
        filled_vol += 1
        
    max_farthest = -1
    max_farthest_index = -1
    for i in range(M):
        farthest = 0
        for j in range(N + 1):
            if n_matrix[i][j] > 0:
                farthest = j
        if farthest > max_farthest:
            max_farthest = farthest
            max_farthest_index = i
            
#    print('Farthest =', max_farthest)
#    print('Farthest for msg', max_farthest_index)
    
    prob_correct_reconstrn = np.sum(n_matrix, axis = 0) / (M * np.array(limits))
    plt.plot(list(range(0, N + 1)), prob_correct_reconstrn)
    
    delta_js_log_f.sort(reverse = True)
    approx_matrix = np.zeros((M, N + 1))
    for i in range(M):
        for j in range(N):
            if delta_js_log_f[i] >= j:
                approx_matrix[i][j] = limits[j]
            else:
                break
    
    prob_rounded = np.sum(approx_matrix, axis = 0) / (M * np.array(limits))
    plt.plot(list(range(0, N + 1)), prob_rounded)        
    
    plt.legend(['Greedy', 'Lagrange rounded'])
#    plt.show()
    plt.savefig('Plots/BitsFlippedvsProbLagrangevsGreedy/' + str(t) +'.svg', format='svg', dpi=1200)
    plt.close()
    plt.figure()
    
    # for putting the legend outside the plot
#    ax = plt.subplot(111)
#    box = ax.get_position()
#    ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
#    legend_x = 1
#    legend_y = 0.5
#    legend_x = 1
#    legend_y = 0.5    
#    legend_list = ['g_' + str(msg_index) + ' = ' + '%.3e' % Decimal(str(msg_probs[msg_index])) for msg_index in msg_indices]
#    plt.legend(legend_list, loc='center left', bbox_to_anchor=(legend_x, legend_y))
#    plt.savefig('Plots/BitsFlippedvsProbForMsgt' + str(t) +'.svg', format='svg', dpi=1200)
#    plt.figure()
    
# save the figure as 3 x 2 plots