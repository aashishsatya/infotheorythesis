import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.special
import scipy.stats as stats
import sys

np.set_printoptions(threshold=sys.maxsize)

def naive_greedy_score(n_matrix, msg_probs):
    
    score = 0.0
    for i in range(M):
        f_i = 0.0
        for j in range(N + 1):
            f_i += n_matrix[i][j] * (p ** j) * ((1 - p) ** (N - j))
        score += msg_probs[i] * math.log2(f_i)
    return score

def compute_updated_exp_val(current_score, N, p, i, j):
    
    """
    Returns <log f>_g as of the entries in the n_matrix
    """
    
    global contrib_msg_to_score
    
    # what has got invalidated is the component resulting from the ith message
    
    updated_score = current_score - msg_probs[i] * math.log2(contrib_msg_to_score[i])
    updated_score += msg_probs[i] * math.log2(contrib_msg_to_score[i] + (p ** j) * ((1 - p)) ** (N - j))
    return updated_score

#Mvals = [100, 150, 200]
#Nvals = list(range(15, 31, 5))
#tvals = list(np.arange(0.5, 5.1, 0.5))
#pvals = [0.15, 0.25, 0.35, 0.45]

# for testing

Mvals = [100]
Nvals = [13]
tvals = [1]
pvals = [0.15]

for M in Mvals:
    for N in Nvals:
        for t in tvals:
            for p in pvals:
#                msg_probs = np.array([j * (2.0/(M * (M - 1))) for j in range(1, M + 1)])
                msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
                #msg_probs = np.random.random(M)
                msg_probs = list(msg_probs)
                msg_probs.sort(reverse = True)
                msg_probs = np.array(msg_probs)
                sum_probs = sum(msg_probs)
                print('Sum of message probs =', sum_probs)
                msg_probs /= sum_probs
                
                contrib_msg_to_score = [0] * M # contribution of each message to the score
                
                p_str = "{:.2f}".format(p)
                t_str = "{:.2f}".format(t)
                
                # do the greedy algorithm
                
                limits = [scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)]
                
                n_matrix = np.zeros((M, N + 1))
                
                filled_vol = 0
                total_vol = 2 ** N
                
                gains_dict = {}
                for i in range(M):
                    for j in range(N + 1):
                        gain = msg_probs[i] * (p ** j) * ((1 - p) ** (N - j))
                        if gain not in gains_dict:
                            gains_dict[gain] = []
                        gains_dict[gain].append((i, j))
                        
                gains_sorted = list(gains_dict.keys())
                gains_sorted.sort(reverse = True)
                
                # limit will only depend on the j value
                
                filled_vol = 0
                gain_index = 0
                while gain_index < M * N and filled_vol < total_vol:
                    gain = gains_sorted[gain_index]
                    for i, j in gains_dict[gain]:
                        if filled_vol + limits[j] <= total_vol:
                            # assign all of them to this one
                            n_matrix[i][j] = limits[j]
                            filled_vol += limits[j]
                        else:
                            n_matrix[i][j] = total_vol - filled_vol
                            filled_vol = total_vol
                    gain_index += 1
                    
                # do the greedy algorithm for exp log f
                
                greedy_n_matrix = np.zeros((M, N + 1))
                filled_vol = 0
            
                # at least one must go into every message
                # otherwise 'log will kill you'
                
                # WLOG assume p < 0.5
                # so it's better to put everything in everything's first one itself
                
                # faster Greedy
                
                current_score = 0
                
                for i in range(M):
                    greedy_n_matrix[i][0] = 1
                    filled_vol += 1
                    # populate contrib_msg_to_score
                    contrib_msg_to_score[i] = (1 - p) ** N
                    current_score += msg_probs[i] * math.log2(contrib_msg_to_score[i])
                    
                L = 2 ** N
                while filled_vol < L:
                    best_gain = -float('inf')
                    best_gain_indices = (-1, -1)
                    for i in range(M):
                        for j in range(N + 1):
                            # search space is always M * (N + 1)
                            if greedy_n_matrix[i][j] == limits[j]:
                                continue
                            new_score = compute_updated_exp_val(current_score, N, p, i, j)
                            if new_score - current_score > best_gain:
                                best_gain = new_score - current_score
                                best_gain_indices = (i, j)
                    i_index, j_index = best_gain_indices
                    contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p) ** (N - j_index))
                    greedy_n_matrix[i_index][j_index] += 1
                    current_score += best_gain
                    filled_vol += 1
                
                # naive Greedy
                
#                naive_greedy_n_matrix = np.zeros((M, N + 1))
#                filled_vol = 0
#                
#                for i in range(M):
#                    naive_greedy_n_matrix[i][0] = 1
#                    filled_vol += 1
#                    
#                print(naive_greedy_n_matrix)
#                    
#                current_score = naive_greedy_score(naive_greedy_n_matrix, msg_probs)
#                nandanam
#                L = 2 ** N
#                while filled_vol < L:
#                    best_gain = -float('inf')
#                    best_gain_indices = (-1, -1)
#                    for i in range(M):
#                        for j in range(N + 1):
#                            if naive_greedy_n_matrix[i][j] == limits[j]:
#                                continue
#                            naive_greedy_n_matrix[i][j] += 1
#                            new_score = naive_greedy_score(naive_greedy_n_matrix, msg_probs)
#                            naive_greedy_n_matrix[i][j] -= 1
#                            gain = new_score - current_score
#                            if gain > best_gain:
#                                best_gain = gain
#                                best_gain_indices = (i, j)
#                    best_i, best_j = best_gain_indices
#                    naive_greedy_n_matrix[best_i][best_j] += 1
#                    filled_vol += 1
#                    print(naive_greedy_n_matrix)
                            
                
                # do the Lagrange approximation to exp log f
                
                lambda_epsilon = 0.001  # epsilon for binary searching lambda
                delta_epsilon = 0.001   # epsilon for binary searching delta
                vol_epsilon = 0.001 # epsilon for volume
                
                lambda_high = 10000.0
                lambda_low = 0
                
                while abs(lambda_low - lambda_high) >= lambda_epsilon:
                    
                    lambda_val = (lambda_high + lambda_low) / 2.0 
                    
                    delta_js_log_f = []   # delta j's
                    
                    for j in range(M):
                    
                        reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
                
                        # find out the value of delta_j for that particular lambda using bisection search
                    
                        high = N
                        low = 0.0
                        delta = 0.0
                        
                        while abs(high - low) >= delta_epsilon:    
                            
                            delta = (high + low) / 2.0
                        
                            exponent = (delta - N / 2.0)**2 / (N / 2)
                            exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                            
                            num = math.e ** exponent
                            
                            denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                            denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                            denom *= 0.5
                            
                            ratio = num / denom
                            
                            if ratio > reqd_ratio:
                                # need smaller delta
                                low = delta
                            else:
                                high = delta               
                        delta_js_log_f.append(delta)
                        
                    # find out the U value for this set of delta j's
                    total_vol = 0.0
                    for j in range(M):
                        vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                        vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                        vol *= 0.5
                        total_vol += vol
                        
                    if abs(total_vol - 1) < vol_epsilon:
                        break
                    
                    if total_vol > 1:
                        # move farther to the right
                        lambda_low = lambda_val
                    else:
                        lambda_high = lambda_val
                        
                
                delta_js_log_f.sort(reverse = True)
                approx_matrix = np.zeros((M, N + 1))
                for i in range(M):
                    for j in range(N):
                        if delta_js_log_f[i] >= j:
                            approx_matrix[i][j] = limits[j]
                        else:
                            break
                
                prob_rounded = np.sum(approx_matrix, axis = 0) / (M * np.array(limits))
                
#                for j in range(M):
#                    f_j = 0.0
#                    for k in range(N + 1):
#                        f_j += approx_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
#                    print('g_' + str(j) + ' * log f_' + str(j) + ' =', msg_probs[j] * math.log2(f_j))
#                    
#                for j in range(M):
#                    f_j = 0.0
#                    for k in range(N + 1):
#                        f_j += approx_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
#                    print('log f_' + str(j) + ' =', math.log2(f_j))
                
                # compute f_j's for each of the messages 
                
                # with log
                
                # Lagrange
                
                fj_with_log = []
                
                for j in range(M):
                    fj = 0
                    for k in range(N + 1):
                        if approx_matrix[j][k] > 0:
                            fj += approx_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
                    fj_with_log.append(fj)
                    
                # Greedy
                
                fj_with_log_greedy = []
                
                for j in range(M):
                    fj = 0
                    for k in range(N + 1):
                        if greedy_n_matrix[j][k] > 0:
                            fj += greedy_n_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
                    fj_with_log_greedy.append(fj)
                    
                print(greedy_n_matrix)
                    
                # naive greedy
                
#                fj_wl_naive_greedy = []
#                for j in range(M):
#                    fj = 0
#                    for k in range(N + 1):
#                        if naive_greedy_n_matrix[j][k] > 0:
#                            fj += naive_greedy_n_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
#                    fj_wl_naive_greedy.append(fj)
                
                # without log 
                    
                fj_wo_log = []
                
                for j in range(M):
                    fj = 0
                    for k in range(N + 1):
                        if n_matrix[j][k] > 0:
                            fj += n_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
                    fj_wo_log.append(fj)
                    
                plt.plot(msg_probs, fj_with_log, alpha = 0.5, marker = 'o', color = 'red')
                plt.plot(msg_probs, fj_with_log_greedy, alpha = 0.5, marker = '^', color = 'green')
#                plt.plot(msg_probs, fj_wl_naive_greedy, alpha = 0.5, marker = '1', color = 'brown')
                plt.plot(msg_probs, fj_wo_log, alpha = 0.5, marker = 'x', color = 'blue')
                plt.title('p_j vs f_j for (M, N, t, p) = ' + str(tuple([M, N, t, float(p_str)])))
                plt.xlabel('Message probabilities')
                plt.ylabel('Probability of error correction')
                plt.ylim([-0.05, 1.05])
                plt.legend(['With log, Lagrange', 'With log, Greedy', 'Without log'])
#                plt.legend(['With log, Lagrange', 'With log, Greedy', 'With log, naive greedy', 'Without log'])
#                plt.show()
                filename = str(M) + '-' + str(N) + '-' + t_str + '-' + p_str
                plt.savefig('Plots/WhyDiscrete/' + filename + '.svg', format = 'svg', dpi = 1200)
                plt.close()
                plt.figure()