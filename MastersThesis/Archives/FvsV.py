import scipy.misc
import matplotlib.pyplot as plt
import numpy as np

p = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
# p = [0.5]
# f = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
f = np.arange(0.01, 1.01, 0.01)

N = 300

for pval in p:
    print('Computing for p =', pval, '...')
    v_vals = []
    for f_val in f:
        # find out the value of delta first
        prob_sum = 0
        reqd_delta = 0
        while reqd_delta <= N:
            prob_sum += int(scipy.special.comb(N, reqd_delta)) * (pval ** reqd_delta) * ((1 - pval) ** (N - reqd_delta))
            if prob_sum > f_val:
                break
            reqd_delta += 1
        # print(reqd_delta)
        v_val = 0
        for i in range(reqd_delta + 1):
            v_val += int(scipy.special.comb(N, i))
        v_val /= (2 ** N)
        v_vals.append(v_val)
    plt.plot(f, v_vals, label = str(pval))
plt.title('F vs V')
plt.xlabel('f(delta_j)')
plt.ylabel('V(delta_j)')
plt.legend([str(pval) for pval in p])
plt.show()
    