import numpy as np
import scipy.misc
#import sys
import matplotlib.pyplot as plt
import math

M = 100
msg_probs = np.random.random(M)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)

N = 12
p = 0.1

n_matrix = np.zeros((M, N + 1))

filled_vol = 0
total_vol = 2 ** N

limits = [scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)]

# at least all the first entries will be zero (otherwise "log will kill you")

for i in range(M):
    n_matrix[i][0] = 1
    filled_vol += 1

while True:
    best_m = 0
    best_n = 0
    best_gain = -float('inf')
    flag = False    # to indicate whether a good m and n have been found
    for i in range(M):
        for j in range(N + 1):
            if n_matrix[i][j] >= limits[j]:
                continue
            gain = msg_probs[i] * (p ** j) * ((1 - p) ** (N - j))
            if gain > best_gain:
                if filled_vol + 1 <= total_vol:
                    flag = True # something has been found
                    best_m = i
                    best_n = j
                    best_gain = gain
    if flag:
        filled_vol += 1
        n_matrix[best_m][best_n] += 1
    else:
        # no new candidates were found
        break

#np.set_printoptions(threshold = sys.maxsize)
#print(n_matrix)
print('Fractional volume filled =', filled_vol / total_vol)
print('Total #decodable sequences =', np.sum(n_matrix))
prob_correct_reconstrn = np.sum(n_matrix, axis = 0) / (M * np.array(limits))
#print(prob_correct_reconstrn)
plt.plot(list(range(0, N + 1)), prob_correct_reconstrn)
plt.title('#bits flipped vs probability of reconstruction')
plt.xlabel('#bits flipped')
plt.ylabel('Probability of correct reconstruction')
plt.show()

exp_val = 0.0
for i in range(M):
    f_i = 0.0
    for j in range(N + 1):
        f_i += n_matrix[i][j] * (p ** j) * ((1 - p) ** (N - j))
    exp_val += msg_probs[i] * f_i
print('Expected probability of error correction =', exp_val)