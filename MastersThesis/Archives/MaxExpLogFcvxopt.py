from cvxopt import matrix, solvers
import numpy as np

pvals = np.array([0.5, 0.1, 0.1, 0.1, 0.2])
n = len(pvals)

c = matrix(-1 * pvals)
b = matrix([1.0])
A = matrix([[1.0],[1.0], [1.0], [1.0], [1.0]])

sol=solvers.lp(c,A,b)
print(sol['x'])