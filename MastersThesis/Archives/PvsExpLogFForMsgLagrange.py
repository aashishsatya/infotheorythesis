import numpy as np
import math
import matplotlib.pyplot as plt
from decimal import Decimal

M = 100
tvals = [1, 2, 3]
#tvals = [1] # for testing purposes
epsilon = 0.0001
error_percentage_threshold = 0.1

N = 15
pvals = list(np.arange(0.05, 0.44, 0.01))
#pvals = list(np.arange(0.05, 0.15, 0.01))   # for testing purposes
L = 2 ** N # for now, say

#msg_indices = list(range(0, M, M // 5))
msg_indices = [0, 1, 2, 3, 4, 5, 10, 25, 50, 75]

prob_arr = {}

for t in tvals:
    
    print('Computing for t =', t, '...')
    msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
    msg_probs = list(msg_probs)
    msg_probs.sort(reverse = True)
    msg_probs = np.array(msg_probs)
    sum_probs = sum(msg_probs)
    msg_probs /= sum_probs
    
    for msg_index in msg_indices:
        prob_arr[msg_index] = []
    
    for p in pvals:
        
        lambda_high = 10000.0
        lambda_low = 0
        
        while abs(lambda_low - lambda_high) >= epsilon / 4:
            
            lambda_val = ( lambda_high + lambda_low) / 2.0 
            
            delta_js_log_f = []   # delta j's
            
            for j in range(M):
            
                reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
        
                # find out the value of delta_j for that particular lambda using bisection search
            
                high = N
                low = 0.0
                delta = 0.0
                
                while abs(low - high) >= epsilon / 2:    # the /2 is arbitrarily chosen
                    
                    delta = (high + low) / 2.0
                
                    exponent = (delta - N / 2.0)**2 / (N / 2)
                    exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                    
                    num = math.e ** exponent
                    
                    denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                    denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                    denom *= 0.5
                    
                    ratio = num / denom
                    
                    percentage_error = abs(ratio - reqd_ratio) * 100 / reqd_ratio
                
                    if percentage_error < error_percentage_threshold:
                        break
                    
                    if ratio > reqd_ratio:
                        # need smaller delta
                        low = delta
                    else:
                        high = delta               
                    
                if abs(low - high) < epsilon / 2:
                    delta_js_log_f.append(0.0)    # because probability was that small
                else:
                    delta_js_log_f.append(delta)
                
            # find out the U value for this set of delta j's
            total_vol = 0.0
            for j in range(M):
                vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                vol *= 0.5
                total_vol += vol
                
            if abs(total_vol - 1) < epsilon:
                break
            
            if total_vol > 1:
                # move farther to the right
                lambda_low = lambda_val
            else:
                lambda_high = lambda_val
                
        if abs(lambda_low - lambda_high) < epsilon / 4:
            print('WARNING: Inappropriate lambda found in case of p = ' + str(p) + '.')
            print('Total volume =', total_vol)
        
        for i in msg_indices:
            f_i = math.erf((delta_js_log_f[i] - N * p) / math.sqrt(2 * N * p * (1 - p)))
            f_i += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
            f_i *= 0.5
            prob_arr[i].append(f_i)  
    
    for msg_index in msg_indices:            
        plt.plot(pvals, prob_arr[msg_index])
    plt.title('P vs Prob Corrn for Msgs (M, N, t) = ' + str(tuple([M, N, t])))
    plt.xlabel('Probability of bit flip (p)')
    plt.ylabel('Prob of correct reconstrn')
    
    # for putting the legend outside the plot
    ax = plt.subplot(111)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
    legend_x = 1
    legend_y = 0.5
    legend_x = 1
    legend_y = 0.5    
    legend_list = ['g_' + str(msg_index) + ' = ' + '%.3e' % Decimal(str(msg_probs[msg_index])) for msg_index in msg_indices]
    plt.legend(legend_list, loc='center left', bbox_to_anchor=(legend_x, legend_y))
    plt.savefig('Plots/PvsExpLogMsg/Lagrange/t' + str(t) +'.svg', format = 'svg', dpi = 1200)
    plt.figure()
    
