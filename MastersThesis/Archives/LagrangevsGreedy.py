import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.special
import scipy.stats as stats

M = 100
msg_probs = np.random.random(M)
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)
msg_probs = np.array(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

N = 12
p = 0.1
epsilon = 0.0001
error_percentage_threshold = 0.1

# plot the normal approximation

mu = N * p
variance = N * p * (1 - p)
sigma = math.sqrt(variance)
x = np.linspace(mu - 3 * sigma, mu + 3 * sigma, 100)
plt.plot(x, N * stats.norm.pdf(x, mu, sigma))

obs_count = [N * scipy.special.comb(N, k, exact = True) * (p ** k) * (1 - p) ** (N - k) for k in range(N + 1)]
plt.bar(list(range(N + 1)), obs_count)

plt.xlabel('Value')
plt.ylabel('Frequency')
plt.title('Normal approximation to the binomial distribution')
plt.show()

lambda_high = 10000.0
lambda_low = 0

while abs(lambda_low - lambda_high) >= epsilon / 4:
    
    lambda_val = ( lambda_high + lambda_low) / 2.0 
    
    delta_js_log_f = []   # delta j's
    
    for j in range(M):
    
        reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]

        # find out the value of delta_j for that particular lambda using bisection search
    
        high = N
        low = 0.0
        delta = 0.0
        
        while abs(low - high) >= epsilon / 2:    # the /2 is arbitrarily chosen
            
            delta = (high + low) / 2.0
        
            exponent = (delta - N / 2.0)**2 / (N / 2)
            exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
            
            num = math.e ** exponent
            
            denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
            denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
            denom *= 0.5
            
            ratio = num / denom
            
            percentage_error = abs(ratio - reqd_ratio) * 100 / reqd_ratio
        
            if percentage_error < error_percentage_threshold:
                break
            
            if ratio > reqd_ratio:
                # need smaller delta
                low = delta
            else:
                high = delta               
            
        if abs(low - high) < epsilon / 2:
            delta_js_log_f.append(0.0)    # because probability was that small
        else:
            delta_js_log_f.append(delta)
        
    # find out the U value for this set of delta j's
    total_vol = 0.0
    for j in range(M):
        vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
        vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
        vol *= 0.5
        total_vol += vol
        
    if abs(total_vol - 1) < epsilon:
        break
    
    if total_vol > 1:
        # move farther to the right
        lambda_low = lambda_val
    else:
        lambda_high = lambda_val
        
if abs(lambda_low - lambda_high) < epsilon / 4:
    print('WARNING: INAPPROPRIATE LAMBDA FOUND IN CASE OF EXP LOG F.')
    print('Total volume =', total_vol)
    
lambda_high = 10000.0
lambda_low = 0

while abs(lambda_low - lambda_high) >= epsilon / 4:
    
    lambda_val = (lambda_high + lambda_low) / 2.0 
    
    delta_js_f = []   # delta j's
    
    for j in range(M):
    
        reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]

        # find out the value of delta_j for that particular lambda using bisection search
    
        high = N
        low = 0.0
        delta = 0.0
        
        while abs(low - high) >= epsilon / 2:    # the /2 is arbitrarily chosen
            
            delta = (high + low) / 2.0
        
            exponent = (delta - N / 2.0)**2 / (N / 2)
            exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
            
            num = math.e ** exponent
            
            denom = 1
            
            ratio = num / denom
            
            percentage_error = abs(ratio - reqd_ratio) * 100 / reqd_ratio
        
            if percentage_error < error_percentage_threshold:
                break
            
            if ratio > reqd_ratio:
                # need smaller delta
                low = delta
            else:
                high = delta               
            
        if abs(low - high) < epsilon / 2:
            delta_js_f.append(0.0)    # because probability was that small
        else:
            delta_js_f.append(delta)
        
    # find out the U value for this set of delta j's
    total_vol = 0.0
    for j in range(M):
        vol = math.erf((delta_js_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
        vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
        vol *= 0.5
        total_vol += vol
        
    if abs(total_vol - 1) < epsilon:
        break
    
    if total_vol > 1:
        # move farther to the right
        lambda_low = lambda_val
    else:
        lambda_high = lambda_val
        
if abs(lambda_low - lambda_high) < epsilon / 4:
    print('WARNING: INAPPROPRIATE LAMBDA FOUND IN CASE OF EXP F.')
    print('Total volume =', total_vol)
        
#print(':')
plt.scatter(msg_probs, delta_js_log_f)
plt.scatter(msg_probs, delta_js_f)
plt.title('Message probabilities and radii')
plt.xlabel('Message probabilities')
plt.ylabel('Radii')
plt.legend(['With log', 'Without log'])
plt.show()

print('')
exp_val = 0.0
for i in range(M):
    f_i = math.erf((delta_js_log_f[i] - N * p) / math.sqrt(2 * N * p * (1 - p)))
    f_i += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
    f_i *= 0.5
#    print('p_' + str(i), '=', msg_probs[i], '; f_' + str(i), '=', f_i)
    exp_val += msg_probs[i] * math.log2(f_i)

print('')
print('<log f>_g = ', exp_val)
print('2^(<log f>_g) = ', 2 ** exp_val)

exp_val = 0.0
for i in range(M):
    f_i = math.erf((delta_js_f[i] - N * p) / math.sqrt(2 * N * p * (1 - p)))
    f_i += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
    f_i *= 0.5
#    print('p_' + str(i), '=', msg_probs[i], '; f_' + str(i), '=', f_i)
    exp_val += msg_probs[i] * f_i
print('<f>_g =', exp_val)

# do the greedy algorithm

n_matrix = np.zeros((M, N + 1))

filled_vol = 0
total_vol = 2 ** N

limits = [scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)]

# at least all the first entries will be zero (otherwise "log will kill you")

for i in range(M):
    n_matrix[i][0] = 1
    filled_vol += 1

while True:
    best_m = 0
    best_n = 0
    best_gain = -float('inf')
    flag = False    # to indicate whether a good m and n have been found
    for i in range(M):
        for j in range(N + 1):
            if n_matrix[i][j] >= limits[j]:
                continue
            gain = msg_probs[i] * (p ** j) * ((1 - p) ** (N - j))
            if gain > best_gain:
                if filled_vol + 1 <= total_vol:
                    flag = True # something has been found
                    best_m = i
                    best_n = j
                    best_gain = gain
    if flag:
        filled_vol += 1
        n_matrix[best_m][best_n] += 1
    else:
        # no new candidates were found
        break

print('\nGreedy algorithm:')
print('Fractional volume filled =', filled_vol / total_vol)
prob_correct_reconstrn = np.sum(n_matrix, axis = 0) / (M * np.array(limits))
#print(prob_correct_reconstrn)
plt.plot(list(range(0, N + 1)), prob_correct_reconstrn)
plt.title('#bits flipped vs probability of reconstruction')
plt.xlabel('#bits flipped')
plt.ylabel('Probability of correct reconstruction')
plt.show()

exp_val = 0.0
for i in range(M):
    f_i = 0.0
    for j in range(N + 1):
        f_i += n_matrix[i][j] * (p ** j) * ((1 - p) ** (N - j))
    exp_val += msg_probs[i] * f_i
print('Expected probability of error correction =', exp_val)
    

    