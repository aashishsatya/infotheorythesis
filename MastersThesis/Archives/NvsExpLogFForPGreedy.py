import numpy as np
import math
import matplotlib.pyplot as plt
from decimal import Decimal

n_choose_k_dict = {}

def compute_score(deltas, msg_probs, p, N):
    
    score = 0.0
    for i in range(len(deltas)):
        delta = deltas[i]
        val = [n_choose_k_dict[(N, k)] * (p ** k) * (1 - p) ** (N - k) for k in range(0, delta + 1)]
        score += msg_probs[i] * math.log2(sum(val))
    return score

def compute_vol(deltas, N):
    
    vol = 0
    for delta in deltas:
        vol += sum([n_choose_k_dict[(N, k)] for k in range(0, delta + 1)])
    return vol

M = 100
t = 1

msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)
msg_probs = np.array(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

Nvals = list(range(10, 30))
#Nvals = list(range(10, 15)) # for testing purposes
pvals = [0.15, 0.25, 0.35, 0.45]
#pvals = [0.15]  # for testing purposes

exp_vals_dict = {}
two_power_exp_vals_dict = {}

for p in pvals:
    
    print('Computing for p =', p, '...')
    exp_vals = []
    two_power_exp_vals = []
    
    for N in Nvals:
        
        print('Computing for N =', N, '...')
        
        L = 2 ** N # for now, say
        
        # populate n_choose_k_dict
            
        for k in range(N + 1):
            n_choose_k_dict[(N, k)] = math.factorial(N) / (math.factorial(N - k) * math.factorial(k))

        # every message needs at least one entry
        # otherwise "log will kill you"
        deltas = list(map(int, list(np.ones((1, M))[0])))
        current_score = compute_score(deltas, msg_probs, p, N)
        
        filled_vol = compute_vol(deltas, N)
        total_vol = L
        flag = True # denotes if a delta has been updated or not
        
        while True:
            flag = False
            best_gain = -float('inf')
            best_gain_index = -1
            for i in range(len(deltas)):
                if deltas[i] == N:
                    # you can't increase this any further
                    continue
                deltas[i] += 1
                if compute_vol(deltas, N) > L:
                    # you can't increase this delta either
                    deltas[i] -= 1
                    continue
                new_score = compute_score(deltas, msg_probs, p, N)
                deltas[i] -= 1  # reset the change
                gain = new_score - current_score
                if gain > best_gain:
                    best_gain = gain
                    best_gain_index = i
                    flag = True
            if not flag:
                # no good gain was found; everything full
                break
            deltas[best_gain_index] += 1
            current_score += best_gain
            
#        print('Fractional volume filled =', compute_vol(deltas, N) / total_vol)
        
        exp_vals.append(current_score)
        two_power_exp_vals.append(2 ** current_score)
        
    exp_vals_dict[p] = exp_vals[:]
    two_power_exp_vals_dict[p] = two_power_exp_vals[:]
        
    
for p in pvals:    
    plt.plot(Nvals, exp_vals_dict[p])

plt.title('N vs <log f>_g')
plt.xlabel('N (Number of bits)')
plt.ylabel('<log f>_g')
legend_list = ['p = ' + str(p) for p in pvals]
plt.legend(legend_list)

plt.savefig('NvsExpLogFForPGreedy.jpg')

plt.figure()

for p in pvals:    
    plt.plot(Nvals, two_power_exp_vals_dict[p])

plt.legend(legend_list)
plt.title('N vs 2 ** <log f>_g')
plt.xlabel('N (Number of bits)')
plt.ylabel('2 ** <log f>_g')

plt.savefig('NvsTwoPowerExpLogFForPGreedy.jpg')