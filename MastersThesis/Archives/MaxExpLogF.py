import cvxpy as cp
import numpy as np

pvals = np.array([0.5, 0.1, 0.1, 0.1, 0.1])
n = len(pvals)
A = np.ones(n).reshape((1, n))
b = np.array([[1]])

x1 = cp.Variable(n)
prob = cp.Problem(cp.Maximize(pvals.T@x),
                 [A@x <= b])
prob.solve()

# Print result.
print("The optimal value is", prob.value)
print("A solution x is")
print(x.value)