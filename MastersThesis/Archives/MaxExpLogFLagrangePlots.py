import numpy as np
import math
import matplotlib.pyplot as plt

msg_probs = np.random.random(100)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

N = 20
prob = 3.99e-05
p = 0.1
epsilon = 0.0001
error_percentage_threshold = 0.1

x_axis = []
numerator = []
denominator = []
ratio = []

for delta in np.arange(0, N + 1, 0.25):
    
    exponent = (delta - N / 2.0)**2 / (N / 2)
    exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
    
    num = math.e ** exponent
    
    diff = math.erf((delta - N * p) / (2 * N * p * (1 - p)))
    diff += math.erf(N * p / (2 * N * p * (1 - p)))
    
    denom = math.sqrt(N * p * (1 - p) / 2) * diff
    
#    print(denom)
    
    if abs(denom - 0) < epsilon:
        print('Denom is zero for delta =', delta)
        continue
    
    denominator.append(denom)
    numerator.append(num)
    ratio.append(num / denom)
    x_axis.append(delta)
    
pred_num = N * (2 * N * p * (1 - p)) - N * p
pred_denom = 2 * (2 * N * p * (1 - p) - N)
pred = pred_num / pred_denom
print('Predicted high value for numerator =', pred)
    
plt.plot(x_axis, numerator, label = 'num')
plt.title('Numerator')
plt.xlabel('delta_j')
plt.ylabel('Value')
plt.figure()

plt.plot(x_axis, denominator, label = 'denom')
plt.title('Denominator')
plt.xlabel('delta_j')
plt.ylabel('Value')
plt.figure()

lambda_val = 5
reqd_value = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / prob

plt.plot(x_axis, ratio, label = 'ratio')
plt.axhline(y = reqd_value, color = 'r', linestyle = '-')
plt.title('Ratio')
plt.xlabel('delta_j')
plt.ylabel('Value')

# all plots in one graph

#plt.figure()
#plt.plot(x_axis, denominator, label = 'denom')
#plt.plot(x_axis, ratio, label = 'ratio')

#plt.title('LHS value')
#plt.xlabel('delta_j')
#plt.ylabel('Value')
#plt.legend(['num', 'denom', 'ratio'])

plt.show()

# find out the value of delta_j for that particular lambda using bisection search
    
high = N
low = 0.0
delta = 0.0

print('Required ratio =', reqd_value)

while abs(low - high) >= epsilon / 2:    # the /2 is arbitrarily chosen
    
    delta = (high + low) / 2.0

    exponent = (delta - N / 2.0)**2 / (N / 2)
    exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
    
    num = math.e ** exponent
    
    diff = math.erf((delta - N * p) / (2 * N * p * (1 - p)))
    diff += math.erf(N * p / (2 * N * p * (1 - p)))
    
    denom = math.sqrt(N * p * (1 - p) / 2) * diff
    
    ratio = num / denom
    
#    print('Ratio =', ratio)
    
    percentage_error = abs(ratio - reqd_value) * 100 / reqd_value

    if percentage_error < error_percentage_threshold:
        break
    
    if ratio > reqd_value:
        # need smaller delta
        low = delta
#        print('New low =', low)
    else:
        high = delta
#        print('New high =', high)               
    
if abs(low - high) < epsilon / 2:
    print('Delta not found.')
else:
    print('Found delta =', delta)
    