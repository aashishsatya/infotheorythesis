import numpy as np
import math
import matplotlib.pyplot as plt

msg_probs = np.random.random(100)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

N = 20
prob = msg_probs[0]
p = 0.1
epsilon = 0.0001

x_axis = []
numerator = []
denominator = []
ratio = []

for delta in range(1, N + 1):
    
    exponent = (delta - N / 2.0)**2 / (N / 2)
    exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
    
    num = math.e ** exponent
    
    diff = math.erf((delta - N * p) / (2 * N * p * (1 - p)))
    diff += math.erf(N * p / (2 * N * p * (1 - p)))
    
    denom = math.sqrt(N * p * (1 - p) / 2) * (diff)
    
#    print(denom)
    
    if abs(denom - 0) < epsilon:
        print('Denom is zero for delta =', delta)
        continue
    
    denominator.append(denom)
    numerator.append(num)
    ratio.append(num / denom)
    x_axis.append(delta)
    
pred_num = N * (2 * N * p * (1 - p)) - N * p
pred_denom = 2 * (2 * N * p * (1 - p) - N)
pred = pred_num / pred_denom
print('Predicted high value for numerator =', pred)
    
plt.plot(x_axis, numerator, label = 'num')
plt.title('Numerator')
plt.xlabel('delta_j')
plt.ylabel('Value')
plt.figure()

plt.plot(x_axis, denominator, label = 'denom')
plt.title('Denominator')
plt.xlabel('delta_j')
plt.ylabel('Value')
plt.figure()

plt.plot(x_axis, ratio, label = 'ratio')
plt.title('Ratio')
plt.xlabel('delta_j')
plt.ylabel('Value')

# all plots in one graph

#plt.figure()
#plt.plot(x_axis, denominator, label = 'denom')
#plt.plot(x_axis, ratio, label = 'ratio')

#plt.title('LHS value')
#plt.xlabel('delta_j')
#plt.ylabel('Value')
#plt.legend(['num', 'denom', 'ratio'])

plt.show()
    