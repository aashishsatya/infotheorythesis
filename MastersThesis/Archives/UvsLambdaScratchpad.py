import numpy as np
import math
import matplotlib.pyplot as plt

msg_probs = np.random.random(20)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs
M = len(msg_probs)

N = 20
p = 0.1
epsilon = 0.0001

lambda_vals = []
total_vols = []

#for lambda_val in np.arange(0.5, 10, 0.5):
for lambda_val in [0.5]:
    
    print('Computing V for lambda =', lambda_val)
    
    delta_js = []   # delta j's
    
    for j in range(M):
    
        reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]

        # find out the value of delta_j for that particular lambda using bisection search
    
        
    