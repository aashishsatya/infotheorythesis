import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.special
import scipy.stats as stats

M = 100
t = 3
msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
#msg_probs = np.random.random(M)
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)
msg_probs = np.array(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

N = 30
p = 0.15

# do the greedy algorithm

limits = [scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)]

n_matrix = np.zeros((M, N + 1))

filled_vol = 0
total_vol = 2 ** N

gains_dict = {}
for i in range(M):
    for j in range(N + 1):
        gain = msg_probs[i] * (p ** j) * ((1 - p) ** (N - j))
        if gain not in gains_dict:
            gains_dict[gain] = []
        gains_dict[gain].append((i, j))
        
gains_sorted = list(gains_dict.keys())
gains_sorted.sort(reverse = True)

#print(gains_dict)

# limit will only depend on the j value

filled_vol = 0
gain_index = 0
while gain_index < M * N and filled_vol < total_vol:
    gain = gains_sorted[gain_index]
    for i, j in gains_dict[gain]:
        if filled_vol + limits[j] <= total_vol:
            # assign all of them to this one
            n_matrix[i][j] = limits[j]
            filled_vol += limits[j]
        else:
            n_matrix[i][j] = total_vol - filled_vol
            filled_vol = total_vol
    gain_index += 1
    

print('\nGreedy algorithm:\n')    
print('Fractional volume filled =', filled_vol / total_vol)
print('2**N =', 2 ** N)
print('Total #decodable sequences =', int(np.sum(n_matrix)))
prob_correct_reconstrn = np.sum(n_matrix, axis = 0) / (M * np.array(limits))
plt.plot(list(range(0, N + 1)), prob_correct_reconstrn)
plt.title('#bits flipped vs reconstruction prob. using Greedy Method')
plt.xlabel('#bits flipped')
plt.ylabel('Probability of correct reconstruction')
plt.show()

exp_val = 0.0
for i in range(M):
    f_i = 0.0
    for j in range(N + 1):
        f_i += n_matrix[i][j] * (p ** j) * ((1 - p) ** (N - j))
    exp_val += msg_probs[i] * f_i
print('Expected probability of error correction =', exp_val)
    

    