import numpy as np
import math
import matplotlib.pyplot as plt

msg_probs = np.random.random(20)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs
M = len(msg_probs)

N = 20
p = 0.1
epsilon = 0.01
error_percentage_threshold = 0.1

lambda_vals = []
total_vols = []

#print('Probs:', msg_probs)

lambda_high = 10000.0
lambda_low = 0

while abs(lambda_low - lambda_high) >= epsilon / 2:
    
    lambda_val = (lambda_high + lambda_low) / 2.0 
    
    print('Lambda low =', lambda_low)
    print('Lambda high =', lambda_high)
    print('Lambda =', lambda_val)
    
    delta_js = []   # delta j's
    
    for j in range(M):
        
        x_axis = []
        numerator = []
        denominator = []
        ratio = []
    
        reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
        
        for delta in np.arange(0.25, N + 1, 0.25):
    
            exponent = (delta - N / 2.0)**2 / (N / 2)
            exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
            
            num = math.e ** exponent
            
            diff = math.erf((delta - N * p) / (2 * N * p * (1 - p)))
            diff += math.erf(N * p / (2 * N * p * (1 - p)))
            
            denom = math.sqrt(N * p * (1 - p) / 2) * diff
            
            if abs(denom - 0) < epsilon:
                print('Denom is zero for delta =', delta)
                continue
            
            denominator.append(denom)
            numerator.append(num)
            ratio.append(num / denom)
            x_axis.append(delta)
            
        plt.plot(x_axis, ratio, label = 'ratio')
        plt.axhline(y = reqd_ratio, color = 'r', linestyle = '-')
        plt.title('Ratio vs delta_j for j = ' + str(j) + '; prob = ' + str(msg_probs[j]))
        plt.xlabel('delta_' + str(j))
        plt.ylabel('Value')
        
        plt.show()
        plt.figure()

    break
        
    if len(delta_js) < M:
        print('Not enough deltas.')
        # move to the right?
        lambda_high = lambda_val
        continue
        
    # find out the U value for this set of delta j's
    total_vol = 0.0
    for j in range(M):
        diff = math.erf((delta_js[j] - N * 0.5) / (2 * N * 0.5 * (1 - 0.5)))
        diff += math.erf(N * 0.5 / (2 * N * 0.5 * (1 - 0.5)))
        vol = math.sqrt(N * 0.5 * (1 - 0.5) / 2) * diff
        total_vol += vol
    print('Total vol =', total_vol)
        
#    lambda_vals.append(lambda_val)
#    total_vols.append(total_vol)
        
    if abs(total_vol - 1) < epsilon:
        print('Found lambda =', lambda_val)
        break
    
    if total_vol > 1:
        # move farther to the right
        lambda_low = lambda_val
    else:
        lambda_high = lambda_val
    
#print('Lambdas:', lambda_vals)
#print('Total volume:', total_vols)

#plt.plot(lambda_vals, total_vols)
#plt.title('U vs lambda')
#plt.axhline(y = 1, color = 'r', linestyle = '-')
#plt.xlabel('lambda')
#plt.ylabel('U value')
#plt.show()
    