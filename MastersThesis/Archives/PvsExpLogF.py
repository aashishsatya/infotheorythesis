import numpy as np
import math
import matplotlib.pyplot as plt

M = 100
tvals = [1, 2, 3]

N = 15
epsilon = 0.0001
error_percentage_threshold = 0.1
pvals = list(np.arange(0.05, 0.44, 0.01))

exp_vals_arr = []
two_power_exp_vals_arr = []

for t in tvals:
    
    print('Computing for t =', t, '...')
    msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
    msg_probs = list(msg_probs)
    msg_probs.sort(reverse = True)
    msg_probs = np.array(msg_probs)
    sum_probs = sum(msg_probs)
    msg_probs /= sum_probs
    
    exp_vals = []
    two_power_exp_vals = []
    
    for p in pvals:
        
        lambda_high = 10000.0
        lambda_low = 0
        
        while abs(lambda_low - lambda_high) >= epsilon / 4:
            
            lambda_val = ( lambda_high + lambda_low) / 2.0 
            
            delta_js_log_f = []   # delta j's
            
            for j in range(M):
            
                reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
        
                # find out the value of delta_j for that particular lambda using bisection search
            
                high = N
                low = 0.0
                delta = 0.0
                
                while abs(low - high) >= epsilon / 2:    # the /2 is arbitrarily chosen
                    
                    delta = (high + low) / 2.0
                
                    exponent = (delta - N / 2.0)**2 / (N / 2)
                    exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                    
                    num = math.e ** exponent
                    
                    denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                    denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                    denom *= 0.5
                    
                    ratio = num / denom
                    
                    percentage_error = abs(ratio - reqd_ratio) * 100 / reqd_ratio
                
                    if percentage_error < error_percentage_threshold:
                        break
                    
                    if ratio > reqd_ratio:
                        # need smaller delta
                        low = delta
                    else:
                        high = delta               
                    
                if abs(low - high) < epsilon / 2:
                    delta_js_log_f.append(0.0)    # because probability was that small
                else:
                    delta_js_log_f.append(delta)
                
            # find out the U value for this set of delta j's
            total_vol = 0.0
            for j in range(M):
                vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                vol *= 0.5
                total_vol += vol
                
            if abs(total_vol - 1) < epsilon:
                break
            
            if total_vol > 1:
                # move farther to the right
                lambda_low = lambda_val
            else:
                lambda_high = lambda_val
                
        if abs(lambda_low - lambda_high) < epsilon / 4:
            print('WARNING: Inappropriate lambda found in case of p = ' + str(p) + '.')
            print('Total volume =', total_vol)
        
          
        exp_val = 0.0
        for i in range(M):
            f_i = math.erf((delta_js_log_f[i] - N * p) / math.sqrt(2 * N * p * (1 - p)))
            f_i += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
            f_i *= 0.5
            if f_i <= 0:
                print('f_i =', f_i, 'for i =', i, 'p_i =', msg_probs[i], 'p =', p)
            exp_val += msg_probs[i] * math.log2(f_i)
        
        exp_vals.append(exp_val)
        two_power_exp_vals.append(2 ** exp_val)
        
    exp_vals_arr.append(exp_vals[:])
    two_power_exp_vals_arr.append(two_power_exp_vals[:])

for i in range(len(tvals)):    
    plt.plot(pvals, exp_vals_arr[i])

plt.title('p vs <log f>_g')
plt.xlabel('p')
plt.ylabel('<log f>_g')
legend_list = ['t = ' + t_str for t_str in list(map(str, tvals))]
plt.legend(legend_list)

plt.figure()

for i in range(len(tvals)):    
    plt.plot(pvals, two_power_exp_vals_arr[i])

plt.legend(legend_list)
plt.title('p vs 2 ** <log f>_g')
plt.xlabel('p')
plt.ylabel('2 ** <log f>_g')