import numpy as np
import math
import matplotlib.pyplot as plt

msg_probs = np.random.random(20)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs
M = len(msg_probs)

N = 20
p = 0.1
epsilon = 0.0001
error_percentage_threshold = 0.1

lambda_vals = []
total_vols = []

lambda_high = 10000.0
lambda_low = 0

while abs(lambda_low - lambda_high) >= epsilon / 2:
    
    lambda_val = (lambda_high + lambda_low) / 2.0 
    
    delta_js = []   # delta j's
    
    for j in range(M):
    
        reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]

        # find out the value of delta_j for that particular lambda using bisection search
    
        high = N
        low = 0.0
        delta = 0.0
        
        while abs(low - high) >= epsilon / 2:    # the /2 is arbitrarily chosen
            
            delta = (high + low) / 2.0
        
            exponent = (delta - N / 2.0)**2 / (N / 2)
            exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
            
            num = math.e ** exponent
            
            diff = math.erf((delta - N * p) / (2 * N * p * (1 - p)))
            diff += math.erf(N * p / (2 * N * p * (1 - p)))
            
            denom = math.sqrt(N * p * (1 - p) / 2) * diff
            
            ratio = num / denom
            
            percentage_error = abs(ratio - reqd_ratio) * 100 / reqd_ratio
        
            if percentage_error < error_percentage_threshold:
                break
            
            if ratio > reqd_ratio:
                # need smaller delta
                low = delta
            else:
                high = delta               
            
        if abs(low - high) < epsilon / 2:
            delta_js.append(0.0)    # because probability was that small
        else:
            delta_js.append(delta)
        
    # find out the U value for this set of delta j's
    total_vol = 0.0
    for j in range(M):
        diff = math.erf((delta_js[j] - N * 0.5) / (2 * N * 0.5 * (1 - 0.5)))
        diff += math.erf(N * 0.5 / (2 * N * 0.5 * (1 - 0.5)))
        vol = math.sqrt(N * 0.5 * (1 - 0.5) / 2) * diff
        total_vol += vol
        
    if abs(total_vol - 1) < epsilon:
#        print('Found lambda =', lambda_val)
        break
    
    if total_vol > 1:
        # move farther to the right
        lambda_low = lambda_val
    else:
        lambda_high = lambda_val
        
#print(':')
plt.scatter(msg_probs, delta_js)
plt.title('Message probabilities and radii')
plt.xlabel('Message probabilities')
plt.ylabel('Radii')
plt.show()

exp_val = 0.0
for i in range(M):
    exp_val += msg_probs[i] * math.log2()

print('<log f>_g = ', exp_val)

    

    