import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.misc

M = 100
t = 1
contrib_msg_to_score = [0] * M # contribution of each message to the score

def compute_updated_exp_val(current_score, N, p, i, j):
    
    """
    Returns <log f>_g as of the entries in the n_matrix
    """
    
    global contrib_msg_to_score
    
    # what has got invalidated is the component resulting from the ith message
    
    updated_score = current_score - msg_probs[i] * contrib_msg_to_score[i]
    updated_score += msg_probs[i] * (contrib_msg_to_score[i] + (p ** j) * ((1 - p)) ** (N - j))
#    if updated_score < current_score:
#        print('Anomaly detected.')
#        print('N, p, i, j =', N, ',', p, ',', i, ',', j)
#        print('New contribution =', contrib_msg_to_score[i] * (p ** j) * ((1 - p)) ** (N - j))
#        assert updated_score > current_score
    return updated_score

msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)
msg_probs = np.array(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

#Nvals = list(range(8, 18))
Nvals = [17]
#Nvals = list(range(10, 13)) # for testing purposes
pvals = [0.15, 0.25, 0.35, 0.45]
#pvals = [0.15]  # for testing purposes

exp_vals_dict = {}
two_power_exp_vals_dict = {}

for p in pvals:
    
    print('Computing for p =', p, '...')
    exp_vals = []
    two_power_exp_vals = []
    
    for N in Nvals:
        
        print('Computing for N =', N, '...')
        
        L = 2 ** N # for now, say
        n_matrix = np.zeros((M, N + 1))
        filled_vol = 0
        total_vol = L
        limits = [scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)]

        # at least one must go into every message
        # otherwise 'log will kill you'
        
        # WLOG assume p < 0.5
        # so its better to put everything in everything's first one itself
        
        current_score = 0
        
        for i in range(M):
            n_matrix[i][0] = 1
            filled_vol += 1
            # populate contrib_msg_to_score
            contrib_msg_to_score[i] = (1 - p) ** N
            current_score += msg_probs[i] * contrib_msg_to_score[i]

        while filled_vol < L:
            best_gain = -float('inf')
            best_gain_indices = (-1, -1)
            for i in range(M):
                for j in range(N + 1):
                    # search space is always M * (N + 1)
                    if n_matrix[i][j] == limits[j]:
                        continue
                    new_score = compute_updated_exp_val(current_score, N, p, i, j)
                    if new_score - current_score > best_gain:
                        best_gain = new_score - current_score
                        best_gain_indices = (i, j)
            i_index, j_index = best_gain_indices
            contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p)) ** (N - j_index)
            n_matrix[i_index][j_index] += 1
            current_score += best_gain
            filled_vol += 1
        
        exp_vals.append(current_score)
        two_power_exp_vals.append(2 ** current_score)
        
    exp_vals_dict[p] = exp_vals[:]
    two_power_exp_vals_dict[p] = two_power_exp_vals[:]
        
    
for p in pvals:    
    plt.plot(Nvals, exp_vals_dict[p])

plt.title('N vs <f>_g')
plt.xlabel('N (Number of bits)')
plt.ylabel('<f>_g')
legend_list = ['p = ' + str(p) for p in pvals]
plt.legend(legend_list)
plt.show()

#plt.savefig('Plots/SanityCheckFasterFinerGreedy.jpg')