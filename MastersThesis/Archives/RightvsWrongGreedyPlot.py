import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.misc

M = 100
t = 3
contrib_msg_to_score = [0] * M # contribution of each message to the score

def compute_updated_exp_val(current_score, N, p, i, j):
    
    """
    Returns <log f>_g as of the entries in the log_f_n_matrix
    """
    
    global contrib_msg_to_score
    
    # what has got invalidated is the component resulting from the ith message
    
    updated_score = current_score - msg_probs[i] * math.log2(contrib_msg_to_score[i])
    updated_score += msg_probs[i] * math.log2(contrib_msg_to_score[i] + (p ** j) * ((1 - p)) ** (N - j))
    return updated_score

msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)
msg_probs = np.array(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

p = 0.15
N = 15

print('Optimizing with log...')

L = 2 ** N # for now, say

log_f_n_matrix = np.zeros((M, N + 1))
filled_vol = 0
total_vol = L
limits = np.array([scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)])

# at least one must go into every message
# otherwise 'log will kill you'

# WLOG assume p < 0.5
# so it's better to put everything in everything's first one itself

current_score = 0

for i in range(M):
    log_f_n_matrix[i][0] = 1
    filled_vol += 1
    # populate contrib_msg_to_score
    contrib_msg_to_score[i] = (1 - p) ** N
    current_score += msg_probs[i] * math.log2(contrib_msg_to_score[i])

print('Total vol =', total_vol)
while filled_vol < L:
    if filled_vol % 2000 == 0:
        print('Filled vol =', filled_vol, '...')
    best_gain = -float('inf')
    best_gain_indices = (-1, -1)
    for i in range(M):
        for j in range(N + 1):
            # search space is always M * (N + 1)
            if log_f_n_matrix[i][j] == limits[j]:
                continue
            new_score = compute_updated_exp_val(current_score, N, p, i, j)
            if new_score - current_score > best_gain:
                best_gain = new_score - current_score
                best_gain_indices = (i, j)
    i_index, j_index = best_gain_indices
    contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p) ** (N - j_index))
    log_f_n_matrix[i_index][j_index] += 1
    current_score += best_gain
    filled_vol += 1
    
# optimizing without log

print('Optimizing without log...')

f_n_matrix = np.zeros((M, N + 1))

filled_vol = 0
total_vol = L

gains_dict = {}
for i in range(M):
    for j in range(N + 1):
        gain = msg_probs[i] * (p ** j) * ((1 - p) ** (N - j))
        if gain not in gains_dict:
            gains_dict[gain] = []
        gains_dict[gain].append((i, j))
        
gains_sorted = list(gains_dict.keys())
gains_sorted.sort(reverse = True)

#print(gains_dict)

# limit will only depend on the j value

filled_vol = 0
gain_index = 0
while gain_index < M * N and filled_vol < total_vol:
    gain = gains_sorted[gain_index]
    for i, j in gains_dict[gain]:
        if filled_vol + limits[j] <= total_vol:
            # assign all of them to this one
            f_n_matrix[i][j] = limits[j]
            filled_vol += limits[j]
        else:
            f_n_matrix[i][j] = total_vol - filled_vol
            filled_vol = total_vol
    gain_index += 1
    
print('Total vol =', total_vol)
print('With log sum =', np.sum(log_f_n_matrix))
print('Without log sum =', np.sum(f_n_matrix))
    
x_axis = list(range(N + 1))

plt.plot(x_axis, np.sum(log_f_n_matrix, axis = 0)/(M * limits))
plt.plot(x_axis, np.sum(f_n_matrix, axis = 0)/(M * limits))
plt.title('No. of bits flipped vs Probability of reconstruction')
plt.legend(['With log', 'Without log'])
plt.xlabel('#bits flipped')
plt.ylabel('Prob of reconstruction')

#plt.savefig('Plots/ExpFvsExpLogFt' + str(t) + 'p' + str(p) + 'N' + str(N) + '.svg', format = 'svg', dpi = 1200)
