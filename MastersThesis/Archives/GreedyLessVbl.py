import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.special

n_choose_k_dict = {}

def compute_score(deltas, msg_probs, p, N):
    
    score = 0.0
    for i in range(len(deltas)):
        delta = deltas[i]
        val = [n_choose_k_dict[(N, k)] * (p ** k) * (1 - p) ** (N - k) for k in range(0, delta + 1)]
        score += msg_probs[i] * math.log2(sum(val))
    return score

def compute_vol(deltas, N):
    
    vol = 0
    for delta in deltas:
        vol += sum([n_choose_k_dict[(N, k)] for k in range(0, delta + 1)])
    return vol

M = 100
t = 1
msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
#msg_probs = np.random.random(M)
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)
msg_probs = np.array(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

N = 15

# populate n_choose_k_dict
    
for k in range(N + 1):
    n_choose_k_dict[(N, k)] = math.factorial(N) / (math.factorial(N - k) * math.factorial(k))
    
pvals = list(np.arange(0.01, 0.51, 0.01))
scores_arr = []
two_power_scores_arr = []
L = 2 ** N # for now, say

# do the greedy algorithm

for p in pvals:
    
    print('Computing for p =', p, '...')

    limits = [scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)]
    
    # every message needs at least one entry
    # otherwise "log will kill you"
    deltas = list(map(int, list(np.ones((1, M))[0])))
    current_score = compute_score(deltas, msg_probs, p, N)
    
    filled_vol = compute_vol(deltas, N)
    total_vol = L
    flag = True # denotes if a delta has been updated or not
    
    while True:
        flag = False
        best_gain = -float('inf')
        best_gain_index = -1
        for i in range(len(deltas)):
            if deltas[i] == N:
                # you can't increase this any further
                continue
            deltas[i] += 1
            if compute_vol(deltas, N) > 2 ** N:
                # you can't increase this delta either
                deltas[i] -= 1
                continue
            new_score = compute_score(deltas, msg_probs, p, N)
            deltas[i] -= 1
            gain = new_score - current_score
            if gain > best_gain:
                best_gain = gain
                best_gain_index = i
                flag = True
        if not flag:
            # no good gain was found; everything full
            break
        deltas[best_gain_index] += 1
        current_score += best_gain
        
    scores_arr.append(current_score)
    two_power_scores_arr.append(2 ** current_score)
        
    print('Fractional volume filled =', compute_vol(deltas, N) / total_vol)

plt.plot(pvals, scores_arr)
plt.title('p vs <log f>_g')
plt.xlabel('p')
plt.ylabel('<log f>_g')


plt.figure()

plt.plot(pvals, two_power_scores_arr)
plt.title('p vs 2 ** <log f>_g')
plt.xlabel('p')
plt.ylabel('2 ** <log f>_g')


plt.show()
    

    