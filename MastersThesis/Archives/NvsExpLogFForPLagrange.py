import numpy as np
import math
import matplotlib.pyplot as plt

n_choose_k_dict = {}

def compute_score(deltas, msg_probs, p, N):
    
    score = 0.0
    for i in range(len(deltas)):
        delta = deltas[i]
        val = [n_choose_k_dict[(N, k)] * (p ** k) * (1 - p) ** (N - k) for k in range(0, delta + 1)]
        score += msg_probs[i] * math.log2(sum(val))
    return score

def compute_vol(deltas, N):
    
    vol = 0
    for delta in deltas:
        vol += sum([n_choose_k_dict[(N, k)] for k in range(0, delta + 1)])
    return vol

M = 250
t = 1

msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)
msg_probs = np.array(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

Nvals = list(range(13, 40))
#Nvals = list(range(10, 15)) # for testing purposes
pvals = [0.15, 0.25, 0.35, 0.45]
#pvals = [0.15]  # for testing purposes
lambda_epsilon = 0.001  # epsilon for binary searching lambda
delta_epsilon = 0.001   # epsilon for binary searching delta
vol_epsilon = 0.001 # epsilon for volume

exp_vals_dict = {}
two_power_exp_vals_dict = {}

for p in pvals:
    
    print('Computing for p =', p, '...')
    exp_vals = []
    two_power_exp_vals = []
    
    for N in Nvals:
        
        print('Computing for N =', N, '...')
        
        L = 2 ** N # for now, say
        
        lambda_high = 10000.0
        lambda_low = 0
        
        while abs(lambda_low - lambda_high) >= lambda_epsilon:
            
            lambda_val = (lambda_high + lambda_low) / 2.0 
            
            delta_js_log_f = []   # delta j's
            
            for j in range(M):
            
                reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
        
                # find out the value of delta_j for that particular lambda using bisection search
            
                high = N
                low = 0.0
                delta = 0.0
                
                while abs(high - low) >= delta_epsilon:    
                    
                    delta = (high + low) / 2.0
                
                    exponent = (delta - N / 2.0)**2 / (N / 2)
                    exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                    
                    num = math.e ** exponent
                    
                    denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                    denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                    denom *= 0.5
                    
                    ratio = num / denom
                    
                    if ratio > reqd_ratio:
                        # need smaller delta
                        low = delta
                    else:
                        high = delta               
                delta_js_log_f.append(delta)
                
            # find out the U value for this set of delta j's
            total_vol = 0.0
            for j in range(M):
                vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                vol *= 0.5
                total_vol += vol
                
            if abs(total_vol - 1) < vol_epsilon:
                break
            
            if total_vol > 1:
                # move farther to the right
                lambda_low = lambda_val
            else:
                lambda_high = lambda_val
                
        exp_val = 0.0
        for i in range(M):
            f_i = math.erf((delta_js_log_f[i] - N * p) / math.sqrt(2 * N * p * (1 - p)))
            f_i += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
            f_i *= 0.5
            exp_val += msg_probs[i] * math.log2(f_i)
            
        exp_vals.append(exp_val)
        two_power_exp_vals.append(2 ** exp_val)
        
    exp_vals_dict[p] = exp_vals[:]
    two_power_exp_vals_dict[p] = two_power_exp_vals[:]
        
    
for p in pvals:    
    plt.plot(Nvals, exp_vals_dict[p])
    
# for putting the legend outside the plot
#legend_x = 1
#legend_y = 0.5
#legend_x = 1
#legend_y = 0.5

plt.title('N vs <log f>_g')
plt.xlabel('N (Number of bits)')
plt.ylabel('<log f>_g')
legend_list = ['p = ' + str(p) for p in pvals]
#ax = plt.subplot(111)
#box = ax.get_position()
#ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
#plt.legend(legend_list, loc='center left', bbox_to_anchor=(legend_x, legend_y))
plt.legend(legend_list)

plt.savefig('NvsExpLogFForPLagrange.jpg')

plt.figure()

for p in pvals:    
    plt.plot(Nvals, two_power_exp_vals_dict[p])
    
#ax = plt.subplot(111)
#box = ax.get_position()
#ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
#plt.legend(legend_list, loc='center left', bbox_to_anchor=(legend_x, legend_y))
plt.title('N vs 2 ** <log f>_g')
plt.xlabel('N (Number of bits)')
plt.ylabel('2 ** <log f>_g')
plt.legend(legend_list)

plt.savefig('NvsTwoPowerExpLogFForPLagrange.jpg')