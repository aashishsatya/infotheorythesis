import numpy as np
import math
import matplotlib.pyplot as plt
from decimal import Decimal

n_choose_k_dict = {}

def compute_score(deltas, msg_probs, p, N):
    
    score = 0.0
    for i in range(len(deltas)):
        delta = deltas[i]
        val = [n_choose_k_dict[(N, k)] * (p ** k) * (1 - p) ** (N - k) for k in range(0, delta + 1)]
        score += msg_probs[i] * math.log2(sum(val))
    return score

def compute_vol(deltas, N):
    
    vol = 0
    for delta in deltas:
        vol += sum([n_choose_k_dict[(N, k)] for k in range(0, delta + 1)])
    return vol

M = 100
tvals = [1, 2, 3]

N = 15
pvals = list(np.arange(0.01, 0.51, 0.01))
L = 2 ** N # for now, say

#msg_indices = list(range(0, M, M // 5))
msg_indices = [0, 1, 2, 3, 4, 5, 10, 25, 50, 75]

prob_arr = {}

# populate n_choose_k_dict
    
for k in range(N + 1):
    n_choose_k_dict[(N, k)] = math.factorial(N) / (math.factorial(N - k) * math.factorial(k))

for t in tvals:
    
    print('Computing for t =', t, '...')
    msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
    msg_probs = list(msg_probs)
    msg_probs.sort(reverse = True)
    msg_probs = np.array(msg_probs)
    sum_probs = sum(msg_probs)
    msg_probs /= sum_probs
    
    for msg_index in msg_indices:
        prob_arr[msg_index] = []
    
    for p in pvals:
    
        print('Computing for p =', p, '...')
    
        # every message needs at least one entry
        # otherwise "log will kill you"
        deltas = list(map(int, list(np.ones((1, M))[0])))
        current_score = compute_score(deltas, msg_probs, p, N)
        
        filled_vol = compute_vol(deltas, N)
        total_vol = L
        flag = True # denotes if a delta has been updated or not
        
        while True:
            flag = False
            best_gain = -float('inf')
            best_gain_index = -1
            for i in range(len(deltas)):
                if deltas[i] == N:
                    # you can't increase this any further
                    continue
                deltas[i] += 1
                if compute_vol(deltas, N) > L:
                    # you can't increase this delta either
                    deltas[i] -= 1
                    continue
                new_score = compute_score(deltas, msg_probs, p, N)
                deltas[i] -= 1  # reset the change
                gain = new_score - current_score
                if gain > best_gain:
                    best_gain = gain
                    best_gain_index = i
                    flag = True
            if not flag:
                # no good gain was found; everything full
                break
            deltas[best_gain_index] += 1
            current_score += best_gain
            
        print('Fractional volume filled =', compute_vol(deltas, N) / total_vol)
        
        for msg_index in msg_indices:
            prob_arr[msg_index].append(sum([n_choose_k_dict[(N, k)] * (p ** k) * (1 - p) ** (N - k) for k in range(0, deltas[msg_index] + 1)]))
    
    
    for msg_index in msg_indices:            
        plt.plot(pvals, prob_arr[msg_index])
    plt.title('p vs <log f>_g for (M, N, t) = ' + str(tuple([M, N, t])))
    plt.xlabel('Probability of bit flip (p)')
    plt.ylabel('Prob of correct reconstrn')
    
    # for putting the legend outside the plot
    ax = plt.subplot(111)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
    legend_x = 1
    legend_y = 0.5
    legend_x = 1
    legend_y = 0.5    
    legend_list = ['g_' + str(msg_index) + ' = ' + '%.3e' % Decimal(str(msg_probs[msg_index])) for msg_index in msg_indices]
    plt.legend(legend_list, loc='center left', bbox_to_anchor=(legend_x, legend_y))
    plt.savefig('Plots/PvsExpLogMsg/Greedy/t' + str(t) + '.svg', format='svg', dpi = 1200)
    plt.figure()
    
