import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import scipy.special
import math

def n_choose_k(N, k):
    return math.factorial(N) / (math.factorial(N - k) * math.factorial(k))

N = 16
p = 0.9

# plot the normal approximation

mu = N * p
variance = N * p * (1 - p)
sigma = math.sqrt(variance)
x = np.linspace(mu - 3 * sigma, mu + 3 * sigma, 100)
plt.plot(x, N * stats.norm.pdf(x, mu, sigma))

obs_count = [N * scipy.special.comb(N, k, exact = True) * (p ** k) * (1 - p) ** (N - k) for k in range(N + 1)]
plt.bar(list(range(N + 1)), obs_count)

plt.xlabel('Value')
plt.ylabel('Frequency')
plt.title('Normal approximation to the binomial distribution')
plt.show()

delta_list = list(range(N + 1))
binom_vol = []
approx_vol = []

for delta in delta_list:
    binom_vol.append(sum([n_choose_k(N, k) for k in range(0, delta + 1)])/2**N)
    vol = math.erf((delta - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
    vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
    vol *= 0.5
    approx_vol.append(vol)
    
plt.plot(delta_list, binom_vol)
plt.plot(delta_list, approx_vol)
plt.legend(['binom', 'approx'])
plt.title('Binomial vs normal approximation of volumes')
plt.xlabel('Delta')
plt.ylabel('Cumulative fractional volume')
