import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.special
import scipy.stats as stats

M = 100
t = 2
msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
#msg_probs = np.random.random(M)
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)
msg_probs = np.array(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs

N = 15
epsilon = 0.0001
error_percentage_threshold = 0.1

exp_vals = []
two_power_exp_vals = []
pvals = list(np.arange(0.03, 0.50, 0.01))

for p in pvals:
    
    print('Computing for p =', p)

    lambda_high = 10000.0
    lambda_low = 0
    
    while abs(lambda_low - lambda_high) >= epsilon / 4:
        
        lambda_val = ( lambda_high + lambda_low) / 2.0 
        
        delta_js_log_f = []   # delta j's
        
        for j in range(M):
        
            reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
    
            # find out the value of delta_j for that particular lambda using bisection search
        
            high = N
            low = 0.0
            delta = 0.0
            
            while abs(low - high) >= epsilon / 2:    # the /2 is arbitrarily chosen
                
                delta = (high + low) / 2.0
            
                exponent = (delta - N / 2.0)**2 / (N / 2)
                exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                
                num = math.e ** exponent
                
                denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                denom *= 0.5
                
                ratio = num / denom
                
                percentage_error = abs(ratio - reqd_ratio) * 100 / reqd_ratio
            
                if percentage_error < error_percentage_threshold:
                    break
                
                if ratio > reqd_ratio:
                    # need smaller delta
                    low = delta
                else:
                    high = delta               
                
            if abs(low - high) < epsilon / 2:
                delta_js_log_f.append(0.0)    # because probability was that small
            else:
                delta_js_log_f.append(delta)
            
        # find out the U value for this set of delta j's
        total_vol = 0.0
        for j in range(M):
            vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
            vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
            vol *= 0.5
            total_vol += vol
            
        if abs(total_vol - 1) < epsilon:
            break
        
        if total_vol > 1:
            # move farther to the right
            lambda_low = lambda_val
        else:
            lambda_high = lambda_val
            
    if abs(lambda_low - lambda_high) < epsilon / 4:
        print('WARNING: Inappropriate lambda found in case of p = ' + str(p) + '.')
        print('Total volume =', total_vol)
    
      
#    plt.scatter(msg_probs, delta_js_log_f)
#    plt.title('Message probabilities and radii')
#    plt.xlabel('Message probabilities')
#    plt.ylabel('Radii')
#    plt.legend(['With log'])
#    plt.show()
    
    exp_val = 0.0
    for i in range(M):
        f_i = math.erf((delta_js_log_f[i] - N * p) / math.sqrt(2 * N * p * (1 - p)))
        f_i += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
        f_i *= 0.5
        if f_i <= 0:
            print('f_i =', f_i, 'for i =', i, 'p_i =', msg_probs[i])
        exp_val += msg_probs[i] * math.log2(f_i)
    
#    print('')
#    print('<log f>_g = ', exp_val)
#    print('2^(<log f>_g) = ', 2 ** exp_val)
        
    exp_vals.append(exp_val)
    two_power_exp_vals.append(2 ** exp_val)
    
plt.plot(pvals, exp_vals)
plt.title('p vs <log f>_g using Lagrange Multipliers')
plt.xlabel('Probability of bit flip (p)')
plt.ylabel('<log f>_g')

plt.figure()
plt.plot(pvals, two_power_exp_vals)
plt.title('p vs 2 ** <log f>_g using Lagrange Multipliers')
plt.xlabel('Probability of bit flip (p)')
plt.ylabel('2 ** <log f>_g')