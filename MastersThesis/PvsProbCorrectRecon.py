import numpy as np
import scipy.misc
#import sys
import matplotlib.pyplot as plt

M = 100
t = 2
msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
#msg_probs = np.random.random(M)
msg_probs = list(msg_probs)
sum_probs = sum(msg_probs)
msg_probs /= sum_probs
msg_probs = list(msg_probs)
msg_probs.sort(reverse = True)

N = 15

limits = [scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)]

p_vals = np.arange(0.01, 0.51, 0.01)
exp_error_corrn = []

for p in p_vals:
    
    n_matrix = np.zeros((M, N + 1))
    
    filled_vol = 0
    total_vol = 2 ** N
    
    gains_dict = {}
    for i in range(M):
        for j in range(N + 1):
            gain = msg_probs[i] * (p ** j) * ((1 - p) ** (N - j))
            if gain not in gains_dict:
                gains_dict[gain] = []
            gains_dict[gain].append((i, j))
            
    gains_sorted = list(gains_dict.keys())
    gains_sorted.sort(reverse = True)
    
    # limit will only depend on the j value
    
    filled_vol = 0
    gain_index = 0
    while gain_index < M * N and filled_vol < total_vol:
        gain = gains_sorted[gain_index]
        for i, j in gains_dict[gain]:
            if filled_vol + limits[j] <= total_vol:
                # assign all of them to this one
                n_matrix[i][j] = limits[j]
                filled_vol += limits[j]
            else:
                n_matrix[i][j] = total_vol - filled_vol
                filled_vol = total_vol
        gain_index += 1
        
    
    exp_val = 0.0
    for i in range(M):
        f_i = 0.0
        for j in range(N + 1):
            f_i += n_matrix[i][j] * (p ** j) * ((1 - p) ** (N - j))
        exp_val += msg_probs[i] * f_i
#    print('Expected probability of error correction =', exp_val)
    exp_error_corrn.append(exp_val)

            
plt.plot(p_vals, exp_error_corrn)
plt.title('P vs Expected Probability of Error Correction')
plt.xlabel('Probability of bit flip')
plt.ylabel('Expected probability of error correction')
#plt.show()
plt.savefig('PvsProbCorrectRecon.jpg')