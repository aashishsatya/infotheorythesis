import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.special
import scipy.stats as stats

def compute_updated_exp_val(current_score, N, p, i, j):
    
    """
    Returns <log f>_g as of the entries in the n_matrix
    """
    
    global contrib_msg_to_score
    
    # what has got invalidated is the component resulting from the ith message
    
    updated_score = current_score - msg_probs[i] * math.log2(contrib_msg_to_score[i])
    updated_score += msg_probs[i] * math.log2(contrib_msg_to_score[i] + (p ** j) * ((1 - p)) ** (N - j))
    return updated_score

Mvals = [100, 150, 200]
Nvals = list(range(15, 31, 5))
tvals = list(np.arange(0.5, 5.1, 0.5))
pvals = [0.15, 0.25, 0.35, 0.45]

for M in Mvals:
    for N in Nvals:
        for t in tvals:
            for p in pvals:
                msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
                #msg_probs = np.random.random(M)
                msg_probs = list(msg_probs)
                msg_probs.sort(reverse = True)
                msg_probs = np.array(msg_probs)
                sum_probs = sum(msg_probs)
                msg_probs /= sum_probs
                
                p_str = "{:.2f}".format(p)
                t_str = "{:.2f}".format(t)
                
                # do the greedy algorithm
                
                # this is for without log
                
                limits = [scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)]
                
                n_matrix = np.zeros((M, N + 1))
                
                filled_vol = 0
                total_vol = 2 ** N
                
                gains_dict = {}
                for i in range(M):
                    for j in range(N + 1):
                        gain = msg_probs[i] * (p ** j) * ((1 - p) ** (N - j))
                        if gain not in gains_dict:
                            gains_dict[gain] = []
                        gains_dict[gain].append((i, j))
                        
                gains_sorted = list(gains_dict.keys())
                gains_sorted.sort(reverse = True)
                
                #print(gains_dict)
                
                # limit will only depend on the j value
                
                filled_vol = 0
                gain_index = 0
                while gain_index < M * N and filled_vol < total_vol:
                    gain = gains_sorted[gain_index]
                    for i, j in gains_dict[gain]:
                        if filled_vol + limits[j] <= total_vol:
                            # assign all of them to this one
                            n_matrix[i][j] = limits[j]
                            filled_vol += limits[j]
                        else:
                            n_matrix[i][j] = total_vol - filled_vol
                            filled_vol = total_vol
                    gain_index += 1
                
                # do the greedy to exp log f
                
                lambda_epsilon = 0.001  # epsilon for binary searching lambda
                delta_epsilon = 0.001   # epsilon for binary searching delta
                vol_epsilon = 0.001 # epsilon for volume
                
                lambda_high = 10000.0
                lambda_low = 0
                
                while abs(lambda_low - lambda_high) >= lambda_epsilon:
                    
                    lambda_val = (lambda_high + lambda_low) / 2.0 
                    
                    delta_js_log_f = []   # delta j's
                    
                    for j in range(M):
                    
                        reqd_ratio = 0.5 * lambda_val * math.sqrt(p * (1 - p)) / msg_probs[j]
                
                        # find out the value of delta_j for that particular lambda using bisection search
                    
                        high = N
                        low = 0.0
                        delta = 0.0
                        
                        while abs(high - low) >= delta_epsilon:    
                            
                            delta = (high + low) / 2.0
                        
                            exponent = (delta - N / 2.0)**2 / (N / 2)
                            exponent -= (delta - N * p)**2 / (2 * N * p * (1 - p))
                            
                            num = math.e ** exponent
                            
                            denom = math.erf((delta - N * p) / math.sqrt(2 * N * p * (1 - p)))
                            denom += math.erf(N * p / math.sqrt(2 * N * p * (1 - p)))
                            denom *= 0.5
                            
                            ratio = num / denom
                            
                            if ratio > reqd_ratio:
                                # need smaller delta
                                low = delta
                            else:
                                high = delta               
                        delta_js_log_f.append(delta)
                        
                    # find out the U value for this set of delta j's
                    total_vol = 0.0
                    for j in range(M):
                        vol = math.erf((delta_js_log_f[j] - N * 0.5) / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                        vol += math.erf(N * 0.5 / math.sqrt(2 * N * 0.5 * (1 - 0.5)))
                        vol *= 0.5
                        total_vol += vol
                        
                    if abs(total_vol - 1) < vol_epsilon:
                        break
                    
                    if total_vol > 1:
                        # move farther to the right
                        lambda_low = lambda_val
                    else:
                        lambda_high = lambda_val
                        
                
                delta_js_log_f.sort(reverse = True)
                approx_matrix = np.zeros((M, N + 1))
                for i in range(M):
                    for j in range(N):
                        if delta_js_log_f[i] >= j:
                            approx_matrix[i][j] = limits[j]
                        else:
                            break
                
                prob_rounded = np.sum(approx_matrix, axis = 0) / (M * np.array(limits))
                
                # compute f_j's for each of the messages 
                
                # with log
                
                fj_with_log = []
                
                for j in range(M):
                    fj = 0
                    for k in range(N + 1):
                        if approx_matrix[j][k] > 0:
                            fj += approx_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
                    fj_with_log.append(fj)
                
                # without log 
                    
                fj_wo_log = []
                
                for j in range(M):
                    fj = 0
                    for k in range(N + 1):
                        if n_matrix[j][k] > 0:
                            fj += n_matrix[j][k] * (p ** k) * ((1 - p) ** (N - k))
                    fj_wo_log.append(fj)
                
                plt.plot(msg_probs, fj_with_log, alpha = 0.5, marker = 'o', color = 'red')
                #plt.scatter(msg_probs, fj_wo_log)
                plt.title('p_j vs f_j for (M, N, t, p) = ' + str(tuple([M, N, t, float(p_str)])))
                plt.xlabel('Message probabilities')
                plt.ylabel('Probability of error correction')
                plt.ylim((0, 1))
                plt.plot(msg_probs, fj_wo_log, alpha = 0.5, marker = 'x', color = 'blue')
                plt.legend(['With log', 'Without log'])
#                plt.show()
                filename = str(M) + '-' + str(N) + '-' + t_str + '-' + p_str
                plt.savefig('Plots/PjvsFj/Greedy/' + filename + '.svg', format = 'svg', dpi = 1200)
                plt.close()
                plt.figure()