import numpy as np
import math
import matplotlib.pyplot as plt
from decimal import Decimal
import scipy.misc

def compute_updated_exp_val(current_score, N, p, i, j):
    
    """
    Returns <log f>_g as of the entries in the n_matrix
    """
    
    global contrib_msg_to_score
    
    # what has got invalidated is the component resulting from the ith message
    
    updated_score = current_score - msg_probs[i] * math.log2(contrib_msg_to_score[i])
    updated_score += msg_probs[i] * math.log2(contrib_msg_to_score[i] + (p ** j) * ((1 - p)) ** (N - j))
    return updated_score

M = 100
tvals = [1, 2, 3]
#tvals = [1] # for testing purposes

N = 15
p = 0.25
L = 2 ** N # for now, say
x_axis = list(range(N + 1))
total_vol = L
limits = np.array([scipy.special.comb(N, k, exact = True) for k in range(0, N + 1)])
contrib_msg_to_score = [0] * M # contribution of each message to the score

#msg_indices = list(range(0, M, M // 5))
msg_indices = [0, 1, 2, 3, 4, 5, 10, 25, 50, 75]

prob_arr = {}

for t in tvals:
    
    print('Computing for t =', t, '...')
    msg_probs = np.array([(1/j) ** t for j in range(1, M + 1)])
    msg_probs = list(msg_probs)
    msg_probs.sort(reverse = True)
    msg_probs = np.array(msg_probs)
    sum_probs = sum(msg_probs)
    msg_probs /= sum_probs
    
    for msg_index in msg_indices:
        prob_arr[msg_index] = []
    
    n_matrix = np.zeros((M, N + 1))
    filled_vol = 0

    # at least one must go into every message
    # otherwise 'log will kill you'
    
    # WLOG assume p < 0.5
    # so it's better to put everything in everything's first one itself
    
    current_score = 0
    
    for i in range(M):
        n_matrix[i][0] = 1
        filled_vol += 1
        # populate contrib_msg_to_score
        contrib_msg_to_score[i] = (1 - p) ** N
        current_score += msg_probs[i] * math.log2(contrib_msg_to_score[i])

    while filled_vol < L:
        best_gain = -float('inf')
        best_gain_indices = (-1, -1)
        for i in range(M):
            for j in range(N + 1):
                # search space is always M * (N + 1)
                if n_matrix[i][j] == limits[j]:
                    continue
                new_score = compute_updated_exp_val(current_score, N, p, i, j)
                if new_score - current_score > best_gain:
                    best_gain = new_score - current_score
                    best_gain_indices = (i, j)
        i_index, j_index = best_gain_indices
        contrib_msg_to_score[i_index] += (p ** j_index) * ((1 - p) ** (N - j_index))
        n_matrix[i_index][j_index] += 1
        current_score += best_gain
        filled_vol += 1
        
    for msg_index in msg_indices:            
        plt.plot(x_axis, n_matrix[msg_index]/limits)
    plt.title('#bits flipped vs prob reconstrn for t = ' + str(t))
    plt.xlabel('#bits flipped')
    plt.ylabel('Prob of correct reconstrn')
    
    # for putting the legend outside the plot
    ax = plt.subplot(111)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
    legend_x = 1
    legend_y = 0.5
    legend_x = 1
    legend_y = 0.5    
    legend_list = ['g_' + str(msg_index) + ' = ' + '%.3e' % Decimal(str(msg_probs[msg_index])) for msg_index in msg_indices]
    plt.legend(legend_list, loc='center left', bbox_to_anchor=(legend_x, legend_y))
    plt.savefig('Plots/BitsFlippedvsProbForMsgt' + str(t) +'.svg', format='svg', dpi=1200)
    plt.figure()
    
