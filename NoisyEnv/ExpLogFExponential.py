import scipy.integrate as integrate
from scipy.special import comb
import numpy as np

def exp_log_f(p, N, Q_exp_log_f, n_choose_k):

    """
    Input: p, the probability of bit-flip; N, the number of cues in the environment; Q_exp_log_f, The matrix Q so far; n_choose_k is nCk for k from 0 to N
    Output: The integrand in the expression for mean
    """

    total = 0

    for i in range(N + 1):
        total += Q_exp_log_f[i] * (p ** i) * ((1 - p) ** (N - i)) / n_choose_k[i]
        # print('Adding', Q_exp_log_f[i] * (p ** i) * ((1 - p) ** (N - i)) / n_choose_k[i])
        # print('Total =', total)

    # if total == 0:
    #     print('Q:', Q_exp_log_f)
    #     print('p, N =', p, N)
    #     for i in range(N):
    #         total += Q_exp_log_f[i] * (p ** i) * ((1 - p) ** (N - i)) / n_choose_k[i]
    #         print('Adding', Q_exp_log_f[i] * (p ** i) * ((1 - p) ** (N - i)) / n_choose_k[i])
    #     print('Total =', total)
    
    return (np.e ** (-p)) * np.log(total) / (1 - (1/np.e))

def maximize_exp_log_f(M, N):

    """
    Input: N, the number of cues in the environment; M, the number of cell types
    Output: An array of integers of length N where the ith index (0-based) is the number of strings i bit-flips away from the intended string that are "rescued"
    """

    n_choose_k = comb(N, range(N + 1))

    quota = int(2 ** N / M)
    Q_exp_log_f = [0] * (N + 1)
    filled = 0

    # first time has to be done separately

    best_gain = -float('inf')
    best_index = -1

    for i in range(N + 1):
        if Q_exp_log_f[i] < n_choose_k[i]:
            Q_exp_log_f[i] += 1
            # print('Q_main:', Q_exp_log_f)
            new_value = integrate.quad(lambda p: exp_log_f(p, N, Q_exp_log_f, n_choose_k), 0, 1)[0]
            gain = new_value
            if gain > best_gain:
                best_gain = gain
                best_index = i
            Q_exp_log_f[i] -= 1
        
    Q_exp_log_f[best_index] += 1
    current_value = integrate.quad(lambda p: exp_log_f(p, N, Q_exp_log_f, n_choose_k), 0, 1)[0]
    filled += 1

    # you have to allocate each string by hand

    while filled < quota:

        best_gain = -float('inf')
        best_index = -1

        for i in range(N + 1):
            if Q_exp_log_f[i] < n_choose_k[i]:
                Q_exp_log_f[i] += 1
                new_value = integrate.quad(lambda p: exp_log_f(p, N, Q_exp_log_f, n_choose_k), 0, 1)[0]
                gain = new_value - current_value
                if gain > best_gain:
                    best_gain = gain
                    best_index = i
                Q_exp_log_f[i] -= 1
        
        Q_exp_log_f[best_index] += 1
        current_value = integrate.quad(lambda p: exp_log_f(p, N, Q_exp_log_f, n_choose_k), 0, 1)[0]
        filled += 1

    return Q_exp_log_f

# M = 5
# N = 13
# print(maximize_exp_log_f(M, N))