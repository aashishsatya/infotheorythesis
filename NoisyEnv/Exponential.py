import ExpFExponential
import ExpLogFExponential

M_array = [5, 10, 15]
N_array = [5, 7, 9, 10, 13]

for M in M_array:
    for N in N_array:
        print('M:', M, 'N:', N)
        print('Maximizing exp f...')
        print(ExpFExponential.maximize_exp_f(M, N))
        print('Maximizing exp log f...')
        print(ExpLogFExponential.maximize_exp_log_f(M, N))
        print('---------------------------------------------------')