import scipy.integrate as integrate
from scipy.special import comb
import numpy as np

def linear(p):

    """
    Linear pdf

        Parameters
        ----------
        p : a float
            The probability of bit flip

        Returns
        -------
        a value:
            The pdf of linear distribution at p

    """

    return 2 * (1 - p)

def exponential(p):

    """
    Exponential pdf

        Parameters
        ----------
        p : a float
            The probability of bit flip

        Returns
        -------
        a value:
            The pdf of exponential distribution at p

    """

    return (np.e ** (-p)) * (1 / (1 - (1/np.e)))

def exp_log_f(distn_function, p, N, Q_exp_log_f, n_choose_k):

    """
    Returns the expression for exp log f (the integrand expression)

        Parameters
        ----------
        distn_function : a function
            The probability distribution function over p
        p : a float
            The probability of bit flip
        N : A positive integer
            Number of cues in the environment
        Q_exp_log_f : A one dimensional array (i.e. list)
            The matrix Q so far
        n_choose_k : A list
            nCk for k from 0 to N

        Returns
        -------
        a value:
            The integrand in the expression for mean when maximizing exp log f

    """

    total = 0

    for i in range(N + 1):
        total += Q_exp_log_f[i] * (p ** i) * ((1 - p) ** (N - i)) / n_choose_k[i]
    
    return distn_function(p) * np.log(total)

def maximize_exp_log_f(M, N, distn_function):

    """
    Returns the matrix Q maximizing exp log f

        Parameters
        ----------
        M: A positive integer
            Number of cell types
        N : A positive integer
            Number of cues in the environment
        distn_function : a function
            The probability distribution function over p

        Returns
        -------
        Q_exp_log_f: An array of integers of length N
            The ith index (0-based) is the number of strings i bit-flips away from the intended string that are "rescued"

    """

    n_choose_k = comb(N, range(N + 1))

    quota = int(2 ** N / M)
    Q_exp_log_f = [0] * (N + 1)
    filled = 0

    # first time has to be done separately

    best_gain = -float('inf')
    best_index = -1

    for i in range(N + 1):
        if Q_exp_log_f[i] < n_choose_k[i]:
            Q_exp_log_f[i] += 1
            new_value = integrate.quad(lambda p: exp_log_f(distn_function, p, N, Q_exp_log_f, n_choose_k), 0, 1)[0]
            gain = new_value
            if gain > best_gain:
                best_gain = gain
                best_index = i
            Q_exp_log_f[i] -= 1
        
    Q_exp_log_f[best_index] += 1
    current_value = integrate.quad(lambda p: exp_log_f(distn_function, p, N, Q_exp_log_f, n_choose_k), 0, 1)[0]
    filled += 1

    # you have to allocate each string by hand

    while filled < quota:

        best_gain = -float('inf')
        best_index = -1

        for i in range(N + 1):
            if Q_exp_log_f[i] < n_choose_k[i]:
                Q_exp_log_f[i] += 1
                new_value = integrate.quad(lambda p: exp_log_f(distn_function, p, N, Q_exp_log_f, n_choose_k), 0, 1)[0]
                gain = new_value - current_value
                if gain > best_gain:
                    best_gain = gain
                    best_index = i
                Q_exp_log_f[i] -= 1
        
        Q_exp_log_f[best_index] += 1
        current_value = integrate.quad(lambda p: exp_log_f(distn_function, p, N, Q_exp_log_f, n_choose_k), 0, 1)[0]
        filled += 1

    return Q_exp_log_f

print(maximize_exp_log_f(7, 10, linear))
print(maximize_exp_log_f(7, 10, exponential))