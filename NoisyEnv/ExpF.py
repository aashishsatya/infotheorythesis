import scipy.integrate as integrate
from scipy.special import comb
import numpy as np

def linear(p):

    """
    Linear pdf

        Parameters
        ----------
        p : a float
            The probability of bit flip

        Returns
        -------
        a value:
            The pdf of linear distribution at p

    """

    return 2 * (1 - p)

def exponential(p):

    """
    Exponential pdf

        Parameters
        ----------
        p : a float
            The probability of bit flip

        Returns
        -------
        a value:
            The pdf of exponential distribution at p

    """

    return (np.e ** (-p)) * (1 / (1 - (1/np.e)))

def exp_f(distn_function, p, k, N):

    """
    Returns the expressionf for exp f (the integrand)

        Parameters
        ----------
        distn_function : a function
            The probability distribution function over p
        p : a float
            The probability of bit flip
        k : a positive integer <= N
            The number of bit flips away from the encoding of a cell type
        N : A positive integer
            Number of cues in the environment

        Returns
        -------
        a value:
            The integrand in the expression for mean when maximizing exp f

    """
    
    return distn_function(p) * (p ** k) * ((1 - p) ** (N - k))

def maximize_exp_f(M, N, distn_function):

    """
    Input: N, the number of cues in the environment; M, the number of cell types
    Output: An array of integers of length N where the ith index (0-based) is the number of strings i bit-flips away from the intended string that are "rescued"
    """

    mean_terms = []

    for i in range(N + 1):
        mean_i_bit_flip = integrate.quad(lambda p: exp_f(distn_function, p, i, N), 0, 1)[0]
        mean_terms.append(mean_i_bit_flip)

    # the bit flips based on val

    bit_flips = list(range(0, N + 1))
    # sorted_bit_flips = [x for _,x in sorted(zip(mean_terms, bit_flips), reverse = True)]
    # courtesy https://gist.github.com/yangshun/ffaf68380ef71c157c3b
    bit_score_tuples = list(zip(mean_terms, bit_flips))
    # print('Bit scores:')
    # for score, bit in bit_score_tuples:
    #     print(bit, ':', score)
    bit_score_tuples.sort(key=lambda x: x[0], reverse=True)
    sorted_bit_flips = [x for _,x in bit_score_tuples]

    quota = int(2 ** N / M)
    filled = 0
    n_choose_k = comb(N, range(N + 1))

    Q_exp_f = [0] * (N + 1)

    for i in range(N + 1):
        if filled + n_choose_k[sorted_bit_flips[i]] <= quota:
            # there is more quota available, so allocate
            Q_exp_f[sorted_bit_flips[i]] = n_choose_k[sorted_bit_flips[i]]
            filled += n_choose_k[sorted_bit_flips[i]]
        else:
            # you can't allocate for the full i
            # so allocate whatever you can
            Q_exp_f[sorted_bit_flips[i]] = quota - filled
            break

    return Q_exp_f

M = 5
N = 13
print(maximize_exp_f(M, N, linear))