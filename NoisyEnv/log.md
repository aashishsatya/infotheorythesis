## Updates

\[Sat Apr  4 13:20:09 IST 2020\] Tried running the program with other values of survival probability of organism (resulting in different values of w). One code still seeems to be the winner. However, some striking results:
- With vastly different good/bad noises (e.g. 0.01.. vs 0.08..), overall probability of survival seems to increase \*exponentially\* with increasing probability of good environment
- In case of relatively similar good/bad noises, the graph was linearly increasing

\[Thu Mar 26 19:48:54 IST 2020\] Added code for constrained Lagrange optimization. Fixed several bugs in the code. Previous day's result is moot in the sense that lambda values obtained are still 0 or 1 (i.e. bet hedging is not encouraged, just preparing for worst case). This is also a backup commit before clearing out the old, unconstrained optimization which set lambda value to 0 and 1 if the optimum was less than 0 or greater than 1 respectively.

\[Tue Mar 24 13:22:03 IST 2020\] Bet hedging seems to be the winner when there are stark differences in noise levels in good and bad environments!

\[Tue Mar 24 12:03:09 IST 2020\] Weird results was because of bug in the code (powers were not being applied correctly). Now the plots are sensible: when there is not much differences in performance between the two settings, plots are nearly the same. Need to check two things now:
- Plots when there is a clear difference in performance ratio
- How the plots look when number of epochs are taken into account

\[Tue Mar 24 03:03:45 IST 2020\] Implemented plot to compare performance in bet-hedging vs having one code. Getting lambda values lt 0 and gt 1 sometimes. Sometimes bet hedging seems to be better and sometimes having one code (see entry below). Seems to depend on good env noise width and bad env noise width (which makes sense). 

\[Sun Mar 15 01:08:22 IST 2020\] Implemented code to find fractional split-up in case of bet hedging. Seems to give optimizing for bad environment as the answer.

\[Mon Feb 24 10:27:05 IST 2020\] Implemented code for maximizing exp f and exp log f accepting pdf as input. Does not seem to be giving different results here either.

\[Sat 22 Feb 21:11:42 IST 2020\] Implemented code for linear distribution of noise. Does not seem to be giving different results when maximizing exp f and exp log f.
